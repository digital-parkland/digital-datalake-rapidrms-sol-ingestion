import argparse
from botocore.exceptions import ClientError
from utilities import constant
from utilities.logging.logger import logger
from utilities.deployment.appdeploy import AppDeploy


def main():
    try:
        parser = argparse.ArgumentParser(description="Deploy CloudFormation templates in config/deploy_config.json.")
        parser.add_argument("--environment", required=True, help="Deployment environment")
        args = parser.parse_args()

        # Set Environment !!! Very important
        constant.ENVIRONMENT = args.environment
        app_d = AppDeploy(environment=args.environment)

        # Deploy KMS and S3 buckets
        if app_d.artifact_deploy.application_name == constant.UTILITIES_APP_NAME:
            app_d.deploy(stack_name=constant.KMS_TEMPLATE_STACK_NAME)
            app_d.deploy(stack_name=constant.S3_TEMPLATE_STACK_NAME)

        # Deploy rest of the templates

        app_d.deploy_artifacts()  # Must deploy artifacts to artifact bucket, before app can be deployed.
        app_d.deploy()  # Deploy based on deploy_config.json

    except ClientError as ex:
        logger.error(__name__, 'ex')
        raise


if __name__ == '__main__':
    main()
