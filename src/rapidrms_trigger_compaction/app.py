import boto3

from utilities import constant
from utilities.etlops.lambdajobutil import LambdaJobUtil


def notification(lambda_etl_job: LambdaJobUtil, *msg):
    l = list(msg)
    s = '/n/r '.join([str(i) for i in l])
    lambda_etl_job.sns_client.publish(msg=s)


def lambda_handler(event, context):
    """

    :param event: { 'glue_job_name': 'rapidrms_compaction_dev'}


    """
    try:
        lambda_etl_job = LambdaJobUtil(context=context)

        lambda_etl_job.logger.info(f"Fetching event {event}")


        gclient = boto3.client('glue')

        response = gclient.start_job_run(
            JobName=event["glue_job_name"],

            Timeout=constant.DEFAULT_GLUE_JOB_TIMEOUT
        )

        lambda_etl_job.logger.info(f"ran etl job - job id: {event['glue_job_name']} - {response}")
        return response

    except Exception as e:

        lambda_etl_job.logger.error(e)
        notification(lambda_etl_job,
                     "ERROR IN Triggering rapidrms compaction job {}".format(
                         lambda_etl_job.etl_job.job_id),
                     "ERROR : {}".format(e))

        raise e
