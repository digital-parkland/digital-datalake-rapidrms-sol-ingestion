import json
import requests
from utilities.clients.secretsmanager import SecretsManagerClient
from utilities.etlops.lambdajobutil import LambdaJobUtil
from awswrangler import s3 as wr
import pandas as pd

def notification(lambda_etl_job: LambdaJobUtil, *msg):
    l = list(msg)
    s = '/n/r '.join([str(i) for i in l])
    lambda_etl_job.sns_client.publish(msg=s)


def get_secret(secret_id):
    try:
        secret_client = SecretsManagerClient()

        secret = secret_client.get_secret_json(secret_id)

        return secret["username"], secret["password"]
    except Exception as e:
        raise e


def get_clients_body(lambda_etl_job, event):
    username, password = get_secret(lambda_etl_job.etl_job.job_args["rapidrms_secret_{}".format(lambda_etl_job.etl_job.job_args["environment"])])

    return {
        "grant_type": "token",
        "client_id": event["client_id"],
        "Username": username,
        "Password": password
    }


def clean_invoices(loc):
    try:
        df = wr.read_csv(loc)
        #KEEPING LAST 30 DAYS OF Invocies to compare for deuplication
        date_subtract = pd.to_datetime("today")-pd.to_timedelta('30D')

        df2=df[pd.to_datetime(df["ingestion_date"])>date_subtract]

        lambda_etl_job.logger.info("Cleaned invoices list have {} invoices and previous invoices have {} records".format(df2.shape[0],df.shape[0]))

        if df2.shape[0]<1:
            return df

        else:
            return df2
    except Exception as e:
        raise e

def lambda_handler(event, context):
    """


    :param event:   {'client_id': 181016, 'client_data': {'client_comname': 'Sol Caribbean Ltd', 'client_storename': 'Sol Caribbean Ltd', 'client_id': 181016, 'client_comcod': 181219, 'client_address': '3rd Floor International Trading Centre, Warrens, St Michael, Barbados', 'client_storeNumber': '000421'}}}
    """

    try:
        global lambda_etl_job
        lambda_etl_job = LambdaJobUtil(context=context)

        lambda_etl_job.logger.info("event: {}".format(event))

        lambda_etl_job.logger.info("Running operation lambda for RapidRMS")

        lambda_etl_job.logger.info(lambda_etl_job.etl_job.job_args)

        lst_invoices = wr.list_directories(lambda_etl_job.etl_job.job_args["invoice_list_path"])

        if lst_invoices:
            for inv in lst_invoices:
                lambda_etl_job.logger.info("Reading invoice list at {}".format(inv))
                cleaned_df= clean_invoices(inv)
                wr.to_csv(cleaned_df,"{}invoices.csv".format(inv),index=False)

        return ""

    except Exception as e:

        lambda_etl_job.logger.error(e)

        notification(lambda_etl_job,
                     "ERROR IN SOL RapidRMS ETL INGESTION LAMBDA {}".format(lambda_etl_job.etl_job.job_id),
                     "ERROR : {}".format(e))

        raise e
