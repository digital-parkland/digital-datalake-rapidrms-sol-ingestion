"""Small script to setup etl object post deployment.

"""
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etlopsutility import run_glue_job
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf


def main(template: TemplateConfig):
    ########################################################################################
    # Entry in the ETL Job table
    # Job arguments passed to the lambda/glue job   Stored in the ETL job table
    ########################################################################################
    pass
    # Get etl_job object and use it to run the glue job.
    # etl_job = EtlJob()
    # etl_job.get_item_by_key(job_id= cf.get_parameter_value_from_param_list(key_name='pEtlJobId', parameter_list=template.parameters))

    # Run the glue job
    # run_glue_job(etl_job=etl_job)
