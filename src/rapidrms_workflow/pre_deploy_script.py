"""Small script to setup etl object pre deployment.

"""
from utilities.clients.s3 import S3Client
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobtype import JobType
from utilities import constant as const, utility
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf
from utilities.clients.ssm import SystemManagerClient
from utilities import constant

def main(template: TemplateConfig):

    pass
    #
    # # Get bucket name from Parameter store
    # ssm_client = SystemManagerClient()
    #
    # # Get information from template configuration (variables provided to DMS cloudformation tempalte)
    # environment = cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)
    # etl_job_id = cf.get_parameter_value_from_param_list(key_name='pEtlJobId', parameter_list=template.parameters)
    # stack_name = template.stack_name_env

    ########################################################################################
    # Entry in the ETL Job table
    # Job arguments passed to the lambda/glue job   Stored in the ETL job table
    ########################################################################################
    # job_args_dic = {
    #     "environment": environment,
    # }
    #
    # # In the job args we are not keeping track of any ingestion markers therefore it is safe to override it on every deployment.
    # etl_job = EtlJob()
    # etl_job.job_id = etl_job_id
    # etl_job.name = etl_job_id
    # etl_job.description = "Rapidrms Workflow with StepFunction"
    # etl_job.job_type = JobType.STEPFUNCTION
    # etl_job.job_args = job_args_dic
    # etl_job.status = 'active'
    # # Must set the function/glue job that is allowed to run this etl job
    # etl_job.function_name = etl_job_id
    # etl_job.add_item()


    ########################################################################################
    # Create prefix in S3 Bucket
