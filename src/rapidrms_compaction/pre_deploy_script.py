"""Small script to setup etl object pre deployment and create s3 bucket prefixes.

"""
import json
from utilities.clients.s3 import S3Client
from utilities.clients.ssm import SystemManagerClient
from utilities import constant
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobtype import JobType
from utilities import constant as const
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf


def main(template: TemplateConfig):
    # Get information from template configuration (variables provided to DMS cloudformation tempalte)
    environment = cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)

    glue_job_name = cf.get_parameter_value_from_param_list(key_name='pGlueJobName', parameter_list=template.parameters)

    pExtraJars = cf.get_parameter_value_from_param_list(key_name='pExtraJars', parameter_list=template.parameters)

    ########################################################################################
    # Add postgresql jar

    artifact_f = open(const.ARTIFACT_DEPLOY_CONFIG_FILE_NAME, "r")
    artifact_file: dict = json.load(artifact_f)

    version_f = open(const.ARTIFACT_VERSION_CONFIG_FILE_NAME, "r")
    version_file: dict = json.load(version_f)

    pExtraJars = f"s3://pki-datalake-artifacts-{environment}/{constant.DEFAULT_ARTIFACT_PREFIX()}/{artifact_file['app_name']}/{version_file[environment]['version']}/jar_files/postgresql-42.2.14.jar,{pExtraJars}"
    cf.set_parameter_value_in_param_list(key_name='pExtraJars', parameter_value=pExtraJars,
                                         parameter_list=template.parameters)

    ########################################################################################
    # Entry in the ETL Job table
    # Job arguments passed to the lambda/glue job   Stored in the ETL job table
    ########################################################################################

    job_args_dic = {
        "environment": environment,
        "path_prefix": "{}/api/rapidrms_invoices/".format(environment),
        "rapidrms_invoices_bucket": "pki-datalake-raw-{}".format(environment),
        "database": "raw_{}".format(environment),
        "table": "rapidrms_invoices",
        "save_path": f"s3://pki-datalake-raw-{environment}/{environment}/api/rapidrms_invoices/",
        "backup_path": f"s3://pki-datalake-raw-{environment}/{environment}/tmp/rapidrms_compaction_backup/",
        "checkpoint_path" : f"s3://pki-datalake-raw-{environment}/{environment}/tmp/rapidrms_compaction_tmp_checkpoint/"

    }

    # In the job args we are not keeping track of any ingestion markers therefore it is safe to override it on every deployment.
    etl_job = EtlJob()
    etl_job.job_id = cf.get_parameter_value_from_param_list(key_name='pEtlJobId', parameter_list=template.parameters)
    etl_job.name = template.stack_name_env
    etl_job.description = "Ingestion job for pic siteProductPriceChanges"
    etl_job.job_type = JobType.GLUE
    etl_job.job_args = job_args_dic
    etl_job.status = 'active'

    # Must set the function/glue job that is allowed to run this etl job
    etl_job.function_name = f"{cf.get_parameter_value_from_param_list(key_name='pGlueJobName', parameter_list=template.parameters)}_{cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)}"
    etl_job.add_item()

    ########################################################################################
    # Create prefix in S3 Bucket
    ssm_client = SystemManagerClient()

    # Temp bucket - prefix
    s3_temp_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/temp/name')
    s3_client = S3Client(url=f"https://{s3_temp_bucket}.s3.amazonaws.com/{glue_job_name}")
    s3_client.check_and_create_prefix(prefix=f'{glue_job_name}')

    # Log bucket - prefix
    s3_temp_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/log/name')
    s3_client = S3Client(url=f"https://{s3_temp_bucket}.s3.amazonaws.com/glue_job_logs/{glue_job_name}")
    s3_client.check_and_create_prefix(prefix=f'glue_job_logs/{glue_job_name}')
