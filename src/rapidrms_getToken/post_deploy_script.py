"""Small script to setup etl object pre deployment.

"""
from utilities.clients.ssm import SystemManagerClient
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf
from utilities.clients.s3 import S3Client
from utilities import constant

def main(template: TemplateConfig):

    pass
    ########################################################################################
    # # Attaches event notification of existing S3 bucket on the user_data prefix
    #
    # # Get information from template configuration (variables provided to DMS cloudformation tempalte)
    # environment = cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)
    # stack_name = template.stack_name_env
    #
    # # Get bucket name from Parameter store
    # ssm_client = SystemManagerClient()
    # s3_target_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/raw/name')
    #
    # s3_url = f"https://{s3_target_bucket}.s3.amazonaws.com/{environment}/user_data"
    #
    # # Get lambda ARN from the cloudformation output
    # lambda_arn = cf.get_export_output(f"{stack_name}-LambdaETLArn")
    # s3_client = S3Client(s3_url)
    #
    # print(f"====  S3 - Lambda trigger ====")
    #
    # print(f"====  S3 - Lambda trigger - Get the current notification configurations ====")
    # response = s3_client._s3client.get_bucket_notification_configuration(Bucket=s3_target_bucket)
    # lambda_configurations = response.get('LambdaFunctionConfigurations', [])
    #
    # # New configuration to add
    # trigger_prefix = f"{environment}/user_data"
    #
    # trigger_id = f'{trigger_prefix.replace("/", "_")}'.strip().lower()
    # new_lambda_config = {
    #     'Id': trigger_id,
    #     'LambdaFunctionArn': lambda_arn,
    #     'Events': [
    #         's3:ObjectCreated:*'
    #     ],
    #     'Filter': {
    #         'Key': {
    #             'FilterRules': [
    #                 {
    #                     'Name': 'Prefix',
    #                     'Value': trigger_prefix
    #                 },
    #                 {
    #                     'Name': 'Suffix',
    #                     'Value': ''
    #                 },
    #             ]
    #         }
    #     }
    # }
    #
    # print(f"====  S3 - Lambda trigger - Check if current config already exits ====")
    # item_index = next((i for i, item in enumerate(lambda_configurations) if item["Id"] == trigger_id), None)
    # if item_index or item_index == 0:
    #     # print(item_index)
    #     lambda_configurations[item_index] = new_lambda_config
    # else:
    #     lambda_configurations.append(new_lambda_config)

    # Save combined configurations

    # try:
    #
    #     print(f"====  S3 - Lambda trigger - update s3  LambdaFunctionConfigurations ====")
    #     response = s3_client._s3client.put_bucket_notification_configuration(
    #         Bucket=s3_target_bucket,
    #         NotificationConfiguration={'LambdaFunctionConfigurations': lambda_configurations}
    #     )
    # except Exception as ex:
    #     print(str(ex))
    #     print(lambda_configurations)
    #     print("Existing lambda config at index", item_index)
    #     raise
