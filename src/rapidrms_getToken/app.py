import json
import requests
from utilities.clients.secretsmanager import SecretsManagerClient
from utilities.etlops.lambdajobutil import LambdaJobUtil


def notification(lambda_etl_job: LambdaJobUtil, *msg):
    l = list(msg)
    s = '/n/r '.join([str(i) for i in l])
    lambda_etl_job.sns_client.publish(msg=s)


def get_secret(secret_id):
    try:
        secret_client = SecretsManagerClient()

        secret = secret_client.get_secret_json(secret_id)

        return secret["username"], secret["password"]
    except Exception as e:
        raise e


def get_clients_body(lambda_etl_job, event):
    username, password = get_secret(lambda_etl_job.etl_job.job_args["rapidrms_secret_{}".format(lambda_etl_job.etl_job.job_args["environment"])])

    return {
        "grant_type": "token",
        "client_id": event["client_id"],
        "Username": username,
        "Password": password
    }


def lambda_handler(event, context):
    """


    :param event:   {'client_id': 181016, 'client_data': {'client_comname': 'Sol Caribbean Ltd', 'client_storename': 'Sol Caribbean Ltd', 'client_id': 181016, 'client_comcod': 181219, 'client_address': '3rd Floor International Trading Centre, Warrens, St Michael, Barbados', 'client_storeNumber': '000421'}}}
    """

    try:

        lambda_etl_job = LambdaJobUtil(context=context)
        lambda_etl_job.logger.info("event: {}".format(event))

        lambda_etl_job.logger.info("Fetching Auth token")

        response = requests.post(lambda_etl_job.etl_job.job_args["generate_token_url"],
                                 json=get_clients_body(lambda_etl_job, event))

        response.raise_for_status()

        response = response.json()
        response_data = json.loads(response["data"])
        event["access_token"] = response_data["access_token"]
        event["DbName"] = response_data["DbName"]

        return event

    except Exception as e:

        lambda_etl_job.logger.error(e)

        notification(lambda_etl_job,
                     "ERROR IN SOL RapidRMS ETL INGESTION LAMBDA {}".format(lambda_etl_job.etl_job.job_id),
                     "ERROR : {}".format(e))

        raise e
