import requests
from utilities.clients.secretsmanager import SecretsManagerClient
from utilities.etlops.lambdajobutil import LambdaJobUtil


def notification(lambda_etl_job: LambdaJobUtil, *msg):
    l = list(msg)
    s = '/n/r '.join([str(i) for i in l])
    lambda_etl_job.sns_client.publish(msg=s)


def prefix_array(dictionary, prefix):
    """Adds prefix value + underscore to beginning of the each dictionary key."""
    return {f"{prefix}_{key}": value for key, value in dictionary.items()}


def get_secret(secret_id):
    try:
        secret_client = SecretsManagerClient()

        secret = secret_client.get_secret_json(secret_id)

        return secret["username"], secret["password"]
    except Exception as e:
        raise e


def get_clients_body(lambda_etl_job):
    username, password = get_secret(lambda_etl_job.etl_job.job_args["rapidrms_secret_{}".format(lambda_etl_job.etl_job.job_args["environment"])])

    return {
        "grant_type": "token",
        "client_id": "0",
        "Username": username,
        "Password": password
    }


def lambda_handler(event, context):
    """

    :param event: {'Input': {'FromDate': '02/23/2021 00:00 AM', 'ToDate': '02/26/2021 00:00 AM'}}

    :return:{'client_id': 181079, 'client_data': {'client_comname': 'Esso', 'client_storename': 'Esso', 'client_id': 181079, 'client_comcod': 182, 'client_address': 'Cul', 'client_storeNumber': '00'}, 'FromDate': '2020', 'ToDate': '2021', 'access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.FFtjxR2F0tiB3GZjl-tsQCI3Y2dmzqM6eVcu4hUNdNM', 'DbName': 'CM2ZSSEH'}

    """
    try:
        lambda_etl_job = LambdaJobUtil(context=context)

        lambda_etl_job.logger.info("Fetching client list")

        response = requests.post(lambda_etl_job.etl_job.job_args["generate_token_url"],
                                 json=get_clients_body(lambda_etl_job))

        response.raise_for_status()

        if response.status_code == 200:

            client_dict = {}
            items_list = []
            for client in response.json()["data"]:
                client_dict = client_dict.copy()
                client.pop("password", None)
                client_data = prefix_array(client, "client")

                if "FromDate" in event["Input"] and "ToDate" in event["Input"]:
                    client_data.update(event["Input"])

                lambda_etl_job.etl_job.job_args["test_clients"] = [int(x) for x in
                                                                   lambda_etl_job.etl_job.job_args["test_clients"]]

                if client_data["client_id"] not in lambda_etl_job.etl_job.job_args["test_clients"]:
                    client_dict["client_id"] = client_data["client_id"]
                    client_dict["client_data"] = client_data
                    items_list.append(client_dict)

            event["items_list"] = items_list
            return event

    except Exception as e:

        lambda_etl_job.logger.error(e)
        notification(lambda_etl_job,
                     "ERROR IN SOL RapidRMS ETL INGESTION LAMBDA {}".format(lambda_etl_job.etl_job.job_id),
                     "ERROR : {}".format(e))

        raise e
