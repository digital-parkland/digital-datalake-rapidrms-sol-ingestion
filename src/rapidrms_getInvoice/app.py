import datetime

import awswrangler as wr
import boto3
import pandas as pd
import requests

from utilities.etlops.lambdajobutil import LambdaJobUtil

boto3.setup_default_session(region_name="us-west-2")


def prefix_array(dictionary, prefix):
    """Adds prefix value + underscore to beginning of the each dictionary key."""
    return {f"{prefix}_{key}": value for key, value in dictionary.items()}


def notification(lambda_etl_job: LambdaJobUtil, *msg):
    l = list(msg)
    s = '/n/r '.join([str(i) for i in l])
    lambda_etl_job.sns_client.publish(msg=s)


def s3_url(bucket, prefix, store=False):
    return "s3://{bucket}/{prefix}".format(
        bucket=bucket, prefix=prefix.rsplit('/', 1)[0]) if store else "s3://{bucket}/{prefix}".format(
        bucket=bucket,
        prefix=prefix)


def store_parquet(dataframe, path, database, table, partition):
    wr.s3.to_parquet(
        df=dataframe,
        path=path,
        dataset=True,
        mode="append",
        database=database,
        table=table,
        compression='gzip', partition_cols=[partition]
    ) if partition else wr.s3.to_parquet(
        df=dataframe,
        path=path,
        dataset=True,
        mode="append",
        database=database,
        table=table,
        compression='gzip'
    )


def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name)
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def get_current_date():
    obj = datetime.datetime.now()
    return obj.date()


def transform(invoices, event, lambda_etl_job):
    # Transform. Flattens every invoice from nested data structure along with renaming its fields and creating new ones.

    df = pd.concat([pd.DataFrame([flatten_json(x) for x in invoices]), pd.DataFrame([event["client_data"]])], axis=1,
                   ignore_index=False)

    df[df.columns[df.isnull().values.all(axis=0)]] = df[df.columns[df.isnull().values.all(axis=0)]].astype("string")

    for k, v in event["client_data"].items():
        df["{}".format(k)] = v

    df["ingestion_date"] = get_current_date()

    df["ingestion_timestamp"] = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]

    df["ingestion_timestamp"] = df["ingestion_timestamp"].astype('datetime64[us]')

    path = "{}/{}/invoices.csv".format(lambda_etl_job.etl_job.job_args["invoice_list_path"],
                                       event["client_data"]["client_id"])

    invoice_present = None

    if wr.s3.does_object_exist(path):
        invoice_present = wr.s3.read_csv(path)
        df = df[~df["invoiceNo"].isin(invoice_present["invoice_no"])]

    return df, invoice_present


def get_invoices(_params, headers, payload, url):
    try:
        req = requests.session()

        _params["pageNo"] = 1
        _params["pageSize"] = 3000

        while True:
            lambda_etl_job.logger.info("Fetching invoices from page no {}".format(_params["pageNo"]))
            response = req.get(
                url,
                params=_params,
                headers=headers,
                data=payload,
                timeout=None
            )
            response.raise_for_status()

            response = response.json()

            if isinstance(response["data"], list):
                yield _params.copy(), response["data"]
                _params["pageNo"] = _params["pageNo"] + 1

            else:
                lambda_etl_job.logger.info("No invoices found on page {}".format(_params["pageNo"]))
                break

    except Exception as e:
        raise e


def invoice_saving_date():
    return get_current_date() - datetime.timedelta(10)


def store_invoices_hist(invoice_df, invoice_present, event, invoice_list_path):
    try:
        invoices_list = invoice_df[["invoice_no", "ingestion_date"]]

        df = pd.concat([invoices_list, invoice_present], ignore_index=True)

        wr.s3.to_csv(df,
                     "{}/{}/invoices.csv".format(invoice_list_path,
                                                 event["client_data"]["client_id"]), index=False)
        lambda_etl_job.logger.info("Saving list of invoices ")
    except Exception as e:
        raise e


def get_params(lambda_etl_job):
    today = datetime.date.today()
    get_invoices_interval = today - datetime.timedelta(int(lambda_etl_job.etl_job.job_args["get_invoices_interval"]))
    time_to_add = " 00:00 AM"
    from_date = get_invoices_interval.strftime("%m/%d/%Y") + time_to_add
    to_date = today.strftime("%m/%d/%Y") + time_to_add
    return {"FromDate": f"{from_date}", "ToDate": f"{to_date}"}


def get_headers(event):
    return {
        "DbName": event["DbName"],
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(event["access_token"]),
    }


def clean_up(invoice_df):
    lambda_etl_job.logger.info("Removing non business columns")
    columns = ['FromDate', 'ToDate', 'totalRows']
    invoice_df.drop(columns, inplace=True, axis=1)

    return invoice_df


def lambda_handler(event, context):
    """

    :param event: {'client_id': 181079, 'client_data': {'client_comname': 'Esso Culloden', 'client_storename': 'Esso Culloden', 'client_id': 181079, 'client_comcod': 181272, 'client_address': 'Culloden Road, St. Michael', 'client_storeNumber': '000467'}, 'access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxODEwNzkiLCJqdGkiOiI1NGY1ZDRlOC0wM2FjLTQ2ZWUtODE2MC01NjQ5MTg5N2E2ODIiLCJpYXQiOiIzLzcvMjAyMSAxMTo1MzoxMCBQTSIsIm5iZiI6MTYxNTE2MTE5MCwiZXhwIjoxNjE1MTYxNzkwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjQ3MjAxL2FwaS92YWx1ZXMiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ3MjAxL2FwaS92YWx1ZXMifQ.i3WS4BpEp_vl6CJ7vesWCyDULOZO5G8tUzwudXFuPaQ', 'DbName': 'CM2ZSSEHDA27JHBNEMJRJSGHKA'}

    :param context:
    :return:
    """

    global lambda_etl_job

    lambda_etl_job = LambdaJobUtil(context=context)

    lambda_etl_job.logger.info("Fetching invoice list")

    lambda_etl_job.logger.info(lambda_etl_job.etl_job.job_args)

    try:

        if not wr.catalog.does_table_exist(database=lambda_etl_job.etl_job.job_args["database"],
                                           table=lambda_etl_job.etl_job.job_args["table"]):

            params = {"FromDate": event["client_data"]["FromDate"], "ToDate": event["client_data"]["ToDate"]}

        else:
            params = get_params(lambda_etl_job)

        lambda_etl_job.logger.info("Parameters: {}".format(params))

        payload = {}
        summary = []
        for param, invoices in get_invoices(params, get_headers(event), payload,
                                            lambda_etl_job.etl_job.job_args["url"]):

            summary.append(
                {"client": event["client_data"]["client_id"], "paramters": param, "invoices_available": len(invoices)})

            invoice_df, invoice_present = transform(invoices, event, lambda_etl_job)

            if len(invoice_df) == 0:
                summary[-1]["invoices_uploaded"] = len(invoice_df)
                summary[-1]["comments"] = "Uploaded {} as {} Invoices already present in the datalake".format(
                    len(invoice_df), len(invoices) - len(invoice_df))

                continue

            store_parquet(clean_up(invoice_df),
                          s3_url(bucket=lambda_etl_job.etl_job.job_args["rapidrms_invoices_bucket"],
                                 prefix=lambda_etl_job.etl_job.job_args["path_prefix"]),
                          lambda_etl_job.etl_job.job_args["database"],
                          lambda_etl_job.etl_job.job_args["table"],
                          "ingestion_date")

            store_invoices_hist(invoice_df, invoice_present, event,
                                lambda_etl_job.etl_job.job_args["invoice_list_path"])

            summary[-1]["invoices_uploaded"] = len(invoice_df)
            summary[-1]["comments"] = "Uploaded {} as {} Invoices already present in the datalake".format(
                len(invoice_df), len(invoices) - len(invoice_df))

        return summary

    except Exception as e:
        lambda_etl_job.logger.error(e)
        #
        notification(lambda_etl_job,
                     "ERROR IN SOL RapidRms ETL INGESTION LAMBDA {}".format(lambda_etl_job.etl_job.job_id),
                     "ERROR : {}".format(e))

        raise e
