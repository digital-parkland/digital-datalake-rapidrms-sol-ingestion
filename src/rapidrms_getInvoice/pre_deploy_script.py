"""Small script to setup etl object pre deployment.

"""
from utilities.clients.s3 import S3Client
from utilities.clients.ssm import SystemManagerClient
from utilities import constant
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobtype import JobType
from utilities import constant as const
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf

def main(template: TemplateConfig):

    # Get information from template configuration (variables provided to DMS cloudformation tempalte)
    environment = cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)
    stack_name = template.stack_name_env


    ########################################################################################
    # Entry in the ETL Job table
    # Job arguments passed to the lambda/glue job   Stored in the ETL job table
    ########################################################################################
    job_args_dic = {
        "environment": environment,
        "url": "https://rapidrmsapi.azurewebsites.net/API/InvoiceReport",
        "rapidrms_secret_{}".format(environment) : "{}/api/rapidrms".format(environment),
        "get_invoices_interval": 7,
        "path_prefix" : "{}/api/rapidrms_invoices/".format(environment),
        "rapidrms_invoices_bucket" : "pki-datalake-raw-{}".format(environment),
        "database": "raw_{}".format(environment),
        "table": "rapidrms_invoices",
        "invoice_list_path" : "s3://pki-datalake-raw-{}/{}/tmp/rapidrms".format(environment,environment)
    }

    # In the job args we are not keeping track of any ingestion markers therefore it is safe to override it on every deployment.
    etl_job = EtlJob()
    etl_job.job_id = cf.get_parameter_value_from_param_list(key_name='pEtlJobId', parameter_list=template.parameters)
    etl_job.name = template.stack_name_env
    etl_job.description = "Lambda for fetching RapidRMS invoices data"
    etl_job.job_type = JobType.LAMBDA
    etl_job.job_args = job_args_dic
    etl_job.status = 'active'

    #
    etl_job.add_item()


    ########################################################################################
    # Create prefix
    #
    # # Get bucket name from Parameter store


    s3_artifact_bucket_name = "pki-datalake-raw-{}".format(environment)

    s3_client = S3Client(url=f"https://{s3_artifact_bucket_name}.s3.amazonaws.com/{environment}")

    s3_client.check_and_create_prefix(prefix=f'{environment}/api')
