"""Small script to setup etl object pre deployment.

"""
from utilities.clients.s3 import S3Client
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobtype import JobType
from utilities import constant as const, utility
from utilities.deployment.appdeploy import TemplateConfig
import utilities.clients.cloudformation as cf
from utilities.clients.ssm import SystemManagerClient
from utilities import constant

def main(template: TemplateConfig):
    pass

    # # Get bucket name from Parameter store
    # ssm_client = SystemManagerClient()
    #
    # # Get information from template configuration (variables provided to DMS cloudformation tempalte)
    # environment = cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)
    # glue_job_name = cf.get_parameter_value_from_param_list(key_name='pGlueJobName', parameter_list=template.parameters)
    # s3prefix = cf.get_parameter_value_from_param_list(key_name='pS3prefix', parameter_list=template.parameters)
    # stack_name = template.stack_name_env
    #
    # s3_source_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/raw/name')
    #
    # s3_target_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/consumption/name')
    # s3_transformed_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/transformed/name')
    #
    #
    # ########################################################################################
    # # Entry in the ETL Job table
    # # Job arguments passed to the lambda/glue job   Stored in the ETL job table
    # ########################################################################################
    # job_args_dic = {
    #     "environment": environment
    # }
    #
    # # In the job args we are not keeping track of any ingestion markers therefore it is safe to override it on every deployment.
    # etl_job = EtlJob()
    # etl_job.job_id = cf.get_parameter_value_from_param_list(key_name='pEtlJobId', parameter_list=template.parameters)
    # etl_job.name = template.stack_name_env
    # etl_job.description = "refer to req. doc. for details: https://parklandfuels.sharepoint.com/:w:/r/sites/D-SM/ED/Shared%20Documents/Digital%20Projects/MTD/MTD_Requirements.docx?d=w4196026e86c24b9993c8350e5ad6254e&csf=1&web=1&e=duedYY "
    # etl_job.job_type = JobType.GLUE
    # etl_job.job_args = job_args_dic
    # etl_job.status = 'active'
    # # Must set the function/glue job that is allowed to run this etl job
    # etl_job.function_name = f"{cf.get_parameter_value_from_param_list(key_name='pGlueJobName', parameter_list=template.parameters)}_{cf.get_parameter_value_from_param_list(key_name='pEnvironment', parameter_list=template.parameters)}"
    # etl_job.add_item()
    #
    #
    # ########################################################################################
    # # Create prefix in S3 Bucket
    #
    # # Temp bucket - prefix
    # s3_temp_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/temp/name')
    # s3_client = S3Client(url=f"https://{s3_temp_bucket}.s3.amazonaws.com/{glue_job_name}")
    # s3_client.check_and_create_prefix(prefix=f'glue_temp')
    # s3_client.check_and_create_prefix(prefix=f'glue_temp/{glue_job_name}')
    #
    # # Log bucket - prefix
    # s3_temp_bucket = ssm_client.get_parameter_value(f'/{environment}/{constant.REGION_NAME}/datalake/s3/log/name')
    # s3_client = S3Client(url=f"https://{s3_temp_bucket}.s3.amazonaws.com/glue_job_logs/{glue_job_name}")
    # s3_client.check_and_create_prefix(prefix=f'glue_job_logs/{glue_job_name}')
    #
    # # Target bucket - prefix
    # s3_client = S3Client(url=f"https://{s3_target_bucket}.s3.amazonaws.com/{environment}")
    # s3_client.check_and_create_prefix(prefix=f'{environment}/{s3prefix}')
    #
