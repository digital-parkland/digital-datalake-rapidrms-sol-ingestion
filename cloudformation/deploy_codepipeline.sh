#!/bin/bash
set -x #echo on

# Run this script from shell
# ./deploy_codepipeline.sh

if [ -z "$1" ]
  then
    echo "environment argument must be provided."
fi

# AWS profile to use and environment variable to use.
environment=$1
default_region=us-west-2

# Get full path
full_path=$(pwd)


#Deploy cloudformation template
template_loation=$full_path/codepipeline.yaml

aws cloudformation deploy --stack-name digital-datalake-rapidRMS-sol-codepipeline-$environment --template-file $template_loation --parameter-overrides pEnvironment=$environment --region $default_region --capabilities CAPABILITY_NAMED_IAM