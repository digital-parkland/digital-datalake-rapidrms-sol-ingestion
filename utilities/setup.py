import setuptools
import os

with open("ReadMe.md", "r") as fh:
    long_description = fh.read()


def read(*rnames):
    return open(
        os.path.join('.', *rnames)
    ).read()

install_requires = [
    a.strip()
    for a in read('requirements/base.txt').splitlines()
    if a.strip() and not a.startswith(('#', '-'))
]


setuptools.setup(
    name="etl-utilities", # Replace with your own username
    version="0.0.1",
    author="San Brar",
    author_email="sanbrar@gmail.com",
    description="Set of utilities to help with etl jobs in AWS",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/none",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    # packages=find_packages('src'),
    # package_dir={'': 'src'},
    include_package_data=True,
)