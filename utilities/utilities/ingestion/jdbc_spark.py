import json
import logging
import os
from enum import Enum
from datetime import timedelta
from utilities import utility
from utilities import utility_datetime
from utilities.clients.s3 import S3Client
from utilities.constant import JDE_SERVER_TIMEZONE_OFFSET

py_logger = logging.getLogger(__name__)
py_logger.setLevel(os.getenv('logging', 'INFO'))

JDBC_BOUND_INT_DB_FETCH_INDICATOR = "-1"
JDBC_BOUND_DATETIME_DB_FETCH_INDICATOR = "1970-01-01"

DEFAULT_TARGET_BATCH_SIZE = "5000000"
DEFAULT_JDBC_BATCH_SIZE_INT = "5000000"
DEFAULT_JDBC_BATCH_SIZE_DAYS = "30"


class JdbcSparkIngestType(Enum):
    """
    Supported ingest strategies
    """
    FULL_LOAD = 'full_load'
    INCREMENTAL = 'incremental'


class JdbcSparkDriver(Enum):
    SQLSERVER = 'com.microsoft.sqlserver.jdbc.SQLServerDriver'
    POSTGRESQL = 'org.postgresql.Driver'


class JdbcSparkPartitionColumnType(Enum):
    """
    Supported source partition column types
    """
    INT = 'int'
    DATE_TIME = 'datetime'
    DATE_TIME2 = 'datetime2'
    DATE_TIME_JDE = 'datetime_jde'
    DATE = 'date'
    NONE = 'N/A'


class SourceDbType(Enum):
    """
    Supported source types
    """
    SQLSERVER = 'sqlserver'
    POSTGRESQL = 'postgresql'


class JdbcSpark:
    """
    Helper class to extract data from jdbc data source using spark.

    Note: When updating any attributes (add or delete) update the from_json and to_json functions
    """

    def __init__(self, log=py_logger, json_dic=None):

        self.log = log

        self.update_etl_job_tbl = True

        self.host = None
        self.port = None
        self.database = None
        self.username = "aaaa"
        self.password = "hi"
        self.ora_service_name = None

        self.source_friendly_name = None
        self.source_connection_secrets_name = None
        self.source_contact_list = None
        self.source_db_type: SourceDbType = SourceDbType.SQLSERVER

        self.driver: JdbcSparkDriver = JdbcSparkDriver.SQLSERVER
        self.jdbc_url = 'jdbc:sqlserver://ms.com:1433;database=san'

        self.ingest_type: JdbcSparkIngestType = JdbcSparkIngestType.FULL_LOAD

        self.source_table_name = None
        self.sql_query = None

        self.jdbc_partition_column = None
        self.jdbc_partition_column_type = JdbcSparkPartitionColumnType.NONE
        self.jdbc_lower_bound = None
        self.jdbc_upper_bound = None
        self.jdbc_batch_size = None

        # self.max_partition_column_value = None

        self.glue_db_name = None
        self.glue_table_name = None

        self.target_s3_bucket = ""
        self.target_s3_prefix = ""
        self.target_s3_path = ""
        self.target_s3_table_name = ""
        self.target_partition_columns = None
        self.target_format = "parquet"  # Currently only supports parquet
        self.target_batch_size = None
        self.target_write_mode = None

        if json_dic:
            self.from_json(json_dic)

            self.post_initialization()

        self.log.info(f"Module: {__name__} -  JDBC Object initialized ======")

    def get_increment_step(self):
        """ Return the increment step

        @return:
        """

        if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
            # Increment by 1
            return 1
        elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME_JDE:
            return 1  # Days

        elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE:
            return 1  # Days

        elif self.jdbc_partition_column_type in [JdbcSparkPartitionColumnType.DATE_TIME,
                                                 JdbcSparkPartitionColumnType.DATE_TIME2]:

            if self.source_db_type == SourceDbType.SQLSERVER and self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME:
                # SQL Server only stores time to approximately 1/300th of a second.
                # These always fall on the 0, 3 and 7 milliseconds. E.g. counting up from 0 in the smallest increment:
                return 3.33  # 3.33 milliseconds
            else:
                return 1  # 1 millisecond

    def post_initialization(self):

        self.target_s3_path = "s3://" + \
                              f"{self.target_s3_bucket}/{self.target_s3_prefix}/{self.target_s3_table_name}/" \
                                  .replace("//", "/")

        # Validate attributes
        self.validate_attributes()
        self.set_target_write_mode()
        self.set_credentials_from_secrets()
        self.set_driver()
        self.set_target_bucket_prefix()

        self.set_jdbc_url()
        self.set_sql_query()

    def validate_attributes(self):
        """
        Validates the attributes
        @return:
        """
        # jdbc upper and lower bounds
        if self.ingest_type == JdbcSparkIngestType.INCREMENTAL:

            if self.jdbc_partition_column is None or self.jdbc_partition_column == "":
                raise NameError(
                    f'validate_attributes - jdbc_partition_column is not valid {self.jdbc_partition_column}')

            if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
                # Check if number can be parsed to integer
                upper_bound = int(float(str(self.jdbc_upper_bound)))
                lower_bound = int(float(str(self.jdbc_lower_bound)))

            elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME_JDE:
                # Value should parse to jde julian date
                upper_bound = utility_datetime.to_jde_julian_date(self.jdbc_upper_bound)
                lower_bound = utility_datetime.to_jde_julian_date(self.jdbc_lower_bound)

            elif self.jdbc_partition_column_type in [JdbcSparkPartitionColumnType.DATE_TIME,
                                                     JdbcSparkPartitionColumnType.DATE,
                                                     JdbcSparkPartitionColumnType.DATE_TIME2]:
                # Value should parse to date
                upper_bound = utility_datetime.to_datetime(self.jdbc_upper_bound)
                lower_bound = utility_datetime.to_datetime(self.jdbc_lower_bound)


            else:
                raise NameError(
                    'validate_attributes - Ingestion type is incremental but partition column type is not valid. Unable to test/validate jdbc_upper_bound and jdbc_lower_bound')

            self.log.info(
                f"Module: {__name__} -  Validation Values: lower_bound = {lower_bound}, upper_bound = {upper_bound} ======")

            batch_size = int(float(str(self.jdbc_batch_size)))

            self.log.info(
                f"Module: {__name__} -  Validation Values: jdbc_batch_size = {batch_size} ======")

        self.log.info(f"Module: {__name__} -  JDBC Object attribute validation completed ======")

    def set_credentials_from_secrets(self):
        self.log.info(f"Module: {__name__} - Get secrets - {self.source_connection_secrets_name} ======")

        secret = json.loads(utility.get_secret(self.source_connection_secrets_name, log=self.log)['SecretString'])
        self.username = secret.get('username', None)
        self.password = secret.get('password', None)
        self.host = secret.get('host', None)
        self.port = secret.get('port', None)
        self.database = secret.get('dbname', None)

    def set_jdbc_url(self):
        # Build URL from individual components
        self.log.info(f"Module: {__name__} - Set/build JDBC URL ======")

        if self.source_db_type == SourceDbType.SQLSERVER:
            self.jdbc_url = f"jdbc:sqlserver://{self.host}:{self.port};databaseName={self.database}"

        elif self.source_db_type == SourceDbType.POSTGRESQL:
            self.jdbc_url = f"jdbc:postgresql://{self.host}:{self.port}/{self.database}"
        else:
            self.jdbc_url = None

        self.log.info(f"Module: {__name__} - URL- {self.jdbc_url} ======")

    def set_driver(self):
        self.log.info(f"Module: {__name__} - Set Driver ======")

        if self.source_db_type == SourceDbType.SQLSERVER:
            self.driver = JdbcSparkDriver.SQLSERVER

        elif self.source_db_type == SourceDbType.POSTGRESQL:
            self.driver = JdbcSparkDriver.POSTGRESQL
        else:
            self.driver = None

        self.log.info(f"Module: {__name__} - DB Driver - {self.driver} ======")

    def set_target_write_mode(self):
        """
        Set target file write mode (overwrite or append)
        """
        if self.target_write_mode is None:
            if self.ingest_type == JdbcSparkIngestType.FULL_LOAD:
                self.target_write_mode = 'overwrite'
            else:
                self.target_write_mode = 'append'

        self.log.info(f"Module: {__name__} - Target Write Mode- {self.target_write_mode} ======")

    def set_sql_query(self):
        self.log.info(f"Module: {__name__} - Set SQL Query ======")

        if self.ingest_type == JdbcSparkIngestType.INCREMENTAL:
            if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
                self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= {self.jdbc_lower_bound} and {self.jdbc_partition_column} <= {self.jdbc_upper_bound}) as t"

            # ### JDE datetime
            elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME_JDE:
                self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= {utility_datetime.to_jde_julian_date(self.jdbc_lower_bound)} and {self.jdbc_partition_column} <= {utility_datetime.to_jde_julian_date(self.jdbc_upper_bound)}) as tbl1"

            # ### date
            elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE:
                if self.source_db_type == SourceDbType.SQLSERVER:
                    self.sql_query = f"(select * from {self.source_table_name} where convert(VARCHAR, {self.jdbc_partition_column}, 23) >= '{self.jdbc_lower_bound[0:10]}' and convert(VARCHAR, {self.jdbc_partition_column}, 23) <= '{self.jdbc_upper_bound[0:10]}' ) as t"
                    self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= CONVERT(date, '{self.jdbc_lower_bound[0:10]}') and {self.jdbc_partition_column} <= CONVERT(date, '{self.jdbc_upper_bound[0:10]}') ) as t"

                elif self.source_db_type == SourceDbType.POSTGRESQL:
                    dt_format = 'YYYY-MM-DD'
                    self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= TO_TIMESTAMP({self.jdbc_lower_bound[0:10]}, '{dt_format}') and {self.jdbc_partition_column} <=  TO_TIMESTAMP({self.jdbc_upper_bound[0:10]}, '{dt_format}')) as t"
                else:
                    raise NameError('Set SQL Error - undefined source db type')

            # ### datetime
            elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME:

                if self.source_db_type == SourceDbType.SQLSERVER:
                    self.sql_query = f"(select * from {self.source_table_name} where convert(VARCHAR, {self.jdbc_partition_column}, 121) >= '{self.jdbc_lower_bound.replace('T', ' ')}' and convert(VARCHAR, {self.jdbc_partition_column}, 121) <= '{self.jdbc_upper_bound.replace('T', ' ')}' ) as t"
                    # f"and {self.jdbc_partition_column} <= convert(datetime, '{self.jdbc_upper_bound.replace('T', ' ')}', 121) " \
                    # f"and  " \
                    # f""
                    # self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= convert(datetime, '{self.jdbc_lower_bound.replace('T', ' ')}', 121) " \
                    #                  f"and {self.jdbc_partition_column} <= convert(datetime, '{self.jdbc_upper_bound.replace('T', ' ')}', 121) " \
                    #                  f"and convert(VARCHAR, {self.jdbc_partition_column}, 121) >= '{self.jdbc_lower_bound.replace('T', ' ')}' " \
                    #                  f") as t"
                elif self.source_db_type == SourceDbType.POSTGRESQL:
                    dt_format = 'YYYY-MM-DD HH24:MI:SS'
                    self.sql_query = f"(select * from {self.source_table_name} where {self.jdbc_partition_column} >= TO_TIMESTAMP({self.jdbc_lower_bound.replace('T', ' ')}, '{dt_format}') and {self.jdbc_partition_column} <=  TO_TIMESTAMP({self.jdbc_upper_bound.replace('T', ' ')}, '{dt_format}')) as t"

                else:
                    raise NameError('Set SQL Error - undefined source db type')
            else:
                print('N/A')
                raise NameError('Set SQL Error - undefined jdbc_partition_column_type')

        elif self.ingest_type == JdbcSparkIngestType.FULL_LOAD:
            self.sql_query = self.source_table_name
        else:
            raise NameError('Set SQL Error - undefined ingest_type')

        self.log.info(f"Module: {__name__} - SQL Query - {self.sql_query} ======")

    def set_defaults(self, env):
        """
        Based on initially entered values this method sets default values for few attributes.
        This is done to make some standardization little easier
        @return:
        """

        self.source_connection_secrets_name = f"{env}/glue/{self.source_friendly_name}"
        self.target_s3_prefix = f"{env}/jdbc/{self.source_friendly_name}/"
        self.target_s3_table_name = self.glue_table_name

        self.target_batch_size = DEFAULT_TARGET_BATCH_SIZE

        if self.ingest_type == JdbcSparkIngestType.INCREMENTAL:

            if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
                self.jdbc_batch_size = DEFAULT_JDBC_BATCH_SIZE_INT
            elif self.jdbc_partition_column_type in [JdbcSparkPartitionColumnType.DATE_TIME,
                                                     JdbcSparkPartitionColumnType.DATE,
                                                     JdbcSparkPartitionColumnType.DATE_TIME2,
                                                     JdbcSparkPartitionColumnType.DATE_TIME_JDE]:
                self.jdbc_batch_size = DEFAULT_JDBC_BATCH_SIZE_DAYS
            else:
                raise NameError('set_defaults - unknown jdbc_partition_column_type')

        self.set_defaults_jdbc_bounds()
        self.post_initialization()

    def set_defaults_jdbc_bounds(self):
        if self.ingest_type == JdbcSparkIngestType.INCREMENTAL:
            if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
                self.jdbc_upper_bound = JDBC_BOUND_INT_DB_FETCH_INDICATOR
                self.jdbc_lower_bound = JDBC_BOUND_INT_DB_FETCH_INDICATOR
            elif self.jdbc_partition_column_type in [JdbcSparkPartitionColumnType.DATE_TIME,
                                                     JdbcSparkPartitionColumnType.DATE,
                                                     JdbcSparkPartitionColumnType.DATE_TIME2,
                                                     JdbcSparkPartitionColumnType.DATE_TIME_JDE]:
                self.jdbc_upper_bound = JDBC_BOUND_DATETIME_DB_FETCH_INDICATOR
                self.jdbc_lower_bound = JDBC_BOUND_DATETIME_DB_FETCH_INDICATOR
            else:
                raise NameError('Set JDBC Bound Defaults - unknown jdbc_partition_column_type')
        else:
            self.jdbc_upper_bound = None
            self.jdbc_lower_bound = None

    def __max_or_min_from_db__(self, spark, max_or_min):
        self.log.info(
            f"Module: {__name__} - get {max_or_min} value - of the partition column {self.jdbc_partition_column} ======")

        # Build sql statement
        sql_query = f"(Select {max_or_min}({self.jdbc_partition_column}) as key_value from {self.source_table_name}) as tm"
        self.log.info(f"Module: {__name__} - get {max_or_min} value - sql query: {sql_query} ======")
        sdf = spark.read.format('jdbc') \
            .option('url', self.jdbc_url) \
            .option('driver', self.driver.value) \
            .option('dbtable', sql_query) \
            .option("user", self.username) \
            .option("password", self.password) \
            .load()

        first_row = sdf.select("key_value").collect()[0]
        self.log.info(f"Module: {__name__} - get {max_or_min} value - first row {str(first_row)} ======")

        if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME:

            ts = first_row.key_value

            if max_or_min == "min":  # For min return one less
                ts = ts - timedelta(milliseconds=self.get_increment_step())

            return_ts = utility_datetime.to_string(dt=ts)[0:23]
            self.log.info(
                f"Module: {__name__} - get {max_or_min} value is: {return_ts} --DateTime-- ======")
            return return_ts

        if self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE:

            dt = first_row.key_value  # Assumption that it is proper date

            if max_or_min == "min":  # For min return one less
                dt = dt - timedelta(days=self.get_increment_step())
                return_dt = utility_datetime.to_string(dt=dt)[0:10]  # Only the date part

            else:  # For Max -- Only can extract previous days transaction
                delta_days = (utility_datetime.now_ts().date() - dt).days
                if delta_days > self.get_increment_step():
                    return_dt = utility_datetime.to_string(dt=dt)[0:10]  # Only the date part
                else:
                    return_dt = utility_datetime.to_string(
                        utility_datetime.now_ts().date() - timedelta(days=self.get_increment_step()))[
                                0:10]  # Only the date part

            self.log.info(f"Module: {__name__} - get {max_or_min} value is: {return_dt} --Date-- ======")
            return return_dt

        elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.DATE_TIME_JDE:
            # JDE Date time
            dt = utility_datetime.to_date_from_jde_julian_date(jde_dt=first_row.key_value)
            now = utility_datetime.now()[0:10]  # Only the date part
            self.log.info(
                f"Module: {__name__} - get {max_or_min} value is: {str(dt)} and now = {now} --JDE-- ======")

            if max_or_min == "min":
                self.log.info(
                    f"Module: {__name__} - get {max_or_min} value fetched is: {str(dt)} and timedelta is: {self.get_increment_step()} --JDE-- ======")
                dt = dt - timedelta(days=self.get_increment_step())
                return utility_datetime.to_string(dt)

            else:
                # Max value can only be yesterday.
                # TODO: Improve JDE process by adding time component
                delta_days = ((utility_datetime.now_ts() - timedelta(hours=JDE_SERVER_TIMEZONE_OFFSET)) - dt).days
                self.log.info(
                    f"Module: {__name__} - get {max_or_min} value fetched is: {str(dt)} and delta_days is: {str(delta_days)} --JDE-- ======")
                if delta_days >= self.get_increment_step():
                    return utility_datetime.to_string(dt)
                else:
                    self.log.info(
                        f"Module: {__name__} - get {max_or_min} value selected: {utility_datetime.to_string(dt - timedelta(days=self.get_increment_step()))} --JDE-- ======")

                    return utility_datetime.to_string(dt - timedelta(days=self.get_increment_step()))

        elif self.jdbc_partition_column_type == JdbcSparkPartitionColumnType.INT:
            # Attempt parsing the value
            a = int(float(str(first_row.key_value)))

            if max_or_min == "min":  # For min return one less
                a = a - self.get_increment_step()

            self.log.info(f"get {max_or_min} value is: {str(a)} --INT-- ======")
            return str(a)

        else:
            raise NameError('__max_or_min_from_db__ - unknown jdbc_partition_column_type')

    def set_jdbc_bound_values_from_db(self, spark):
        """
        Get the max or min value for the partition column
        @param spark:
        @return: None
        """

        self.log.info(
            f"Module: {__name__} - get upper and lower from db ======")

        if self.ingest_type == JdbcSparkIngestType.INCREMENTAL:
            if self.jdbc_lower_bound in [JDBC_BOUND_INT_DB_FETCH_INDICATOR, JDBC_BOUND_DATETIME_DB_FETCH_INDICATOR]:
                self.jdbc_lower_bound = self.__max_or_min_from_db__(spark=spark, max_or_min="min")

            if self.jdbc_upper_bound in [JDBC_BOUND_INT_DB_FETCH_INDICATOR, JDBC_BOUND_DATETIME_DB_FETCH_INDICATOR]:
                self.jdbc_upper_bound = self.__max_or_min_from_db__(spark=spark, max_or_min="max")

    def set_target_bucket_prefix(self):
        # Glue target bucket
        # self.log.info(f"Module: {__name__} - creating/checking prefix {self.target_s3_prefix} ======")

        check_prefix = self.target_s3_path.replace(f"s3://{self.target_s3_bucket}/", "").replace("//", "/")

        s3_client = S3Client(url=f"https://{self.target_s3_bucket}.s3.amazonaws.com/{check_prefix}")
        s3_client.check_and_create_prefix(prefix=check_prefix)

        pass

    def to_json(self, additional_exclude_columns: [] = None) -> str:
        """
        Returns current object in json formatted string
        :return:
        """
        exclude_column_list = ["log", "password", "source_db_type", "ingest_type", "jdbc_partition_column_type",
                               "driver"]

        if additional_exclude_columns:
            exclude_column_list.extend(additional_exclude_columns)

        filtered = {k: v for k, v in self.__dict__.items() if (v is not None and k not in exclude_column_list)}

        j = json.dumps(filtered, indent=4, cls=utility.DecimalEncoder)

        json_obj = j[:len(j) - 1] + \
                   ', "source_db_type": "' + str(self.source_db_type.value) + '"' + \
                   ', "ingest_type": "' + str(self.ingest_type.value) + '"' + \
                   ', "jdbc_partition_column_type": "' + str(self.jdbc_partition_column_type.value) + '"' + \
                   '}'

        return json_obj

    def to_json_eltjob_args(self):
        return json.loads(
            self.to_json(
                additional_exclude_columns=["host", "database", "driver", 
                                            "jdbc_url", 
                                            "ora_service_name", 
                                            "port", 
                                            "sql_query", "target_format", 
                                            "update_etl_job_tbl",
                                            "username", "target_s3_path"]
            )
        )

    def from_json(self, json_obj):
        """
        Initialize the JDBC Spark object from dictionary object. Can pass on json string as well.
        @param json_obj:
        @return:
        """
        self.log.info(f"Module: {__name__} -  Attempt to initialize jdbc spark object from  json object ======")

        if isinstance(json_obj, str):
            json_obj = json.loads(json_obj)

        self.log.info(json.dumps(json_obj, indent=4, cls=utility.DecimalEncoder))

        allowed_keys = {'source_friendly_name', 'source_connection_secrets_name', 'update_etl_job_tbl',
                        'source_table_name', 'source_contact_list',
                        'jdbc_partition_column', 'jdbc_partition_column_type', 'jdbc_lower_bound', 'jdbc_upper_bound',
                        'jdbc_batch_size', 'glue_db_name', 'glue_table_name', 'target_s3_bucket', 'target_s3_prefix',
                        'target_s3_table_name', 'target_partition_columns', 'target_batch_size', 'target_write_mode'}

        self.__dict__.update((k, v) for k, v in json_obj.items() if k in allowed_keys)

        source_db_type = json_obj.get('source_db_type')
        self.source_db_type = SourceDbType(source_db_type) if source_db_type else None

        ingest_type = json_obj.get('ingest_type')
        self.ingest_type = JdbcSparkIngestType(ingest_type) if ingest_type else None

        jdbc_partition_column_type = json_obj.get('jdbc_partition_column_type')
        self.jdbc_partition_column_type = JdbcSparkPartitionColumnType(
            jdbc_partition_column_type) if jdbc_partition_column_type else None
