"""Set of utilities

Revisions:
    2020-09-19 San Brar    Initial version

"""
import os
import shutil
import sys
import json
from zipfile import ZipFile
import subprocess

from botocore.exceptions import ClientError

from utilities.clients.ssm import SystemManagerClient
from utilities.logging.logger import logger
from utilities import utility
from utilities import constant
from utilities.utility import make_archive

class UtilitiesPackage:
    """Package and deploy utilities
        Create a zip of a given python package. must have following structure
         - package_name
            - LICENSE
            - digital_datalake_athena_workgroups.md
            - setup,py
            - tests
            - package_name
                - __init__.py (can be blank)
                - *.py x number of code files
        @param package_name: Name of the package and package folder.
        @param target_folder:
    """

    def __init__(self):
        # Find utilities folder location
        self.package_name = "utilities"

        # Using relative path determine the utilities folder.
        if os.path.exists(f"{self.package_name}"):
            self.relative_location = self.package_name
        elif os.path.exists(f"./../{self.package_name}"):
            self.relative_location = f"./../{self.package_name}"
        elif os.path.exists(f"./../../{self.package_name}"):
            self.relative_location = f"./../../{self.package_name}"
        else:
            raise NameError(f"Unable to find location of the {self.package_name} package.")

        self.lambda_path = self.relative_location + "/_lambda_build_utilities"

    def build(self):
        """Build local utilities package

        :return:
        """
        """
        Create a zip of a given python package. must have following structure
         - package_name
            - LICENSE
            - digital_datalake_athena_workgroups.md
            - setup,py
            - tests
            - package_name
                - __init__.py (can be blank)
                - *.py x number of code files
        @param package_name: Name of the package and package folder.
        @param target_folder:
        """

        logger.info(self.__class__.__name__, "SAM - build", self.package_name)

        build_p = subprocess.run(['sam', 'build'], cwd=self.lambda_path, capture_output=True)

        logger.info(self.__class__.__name__, build_p.stdout, self.package_name)
        if build_p.returncode != 0:
            logger.error(self.__class__.__name__, build_p.stderr, self.package_name)
            raise Exception(f"{self.package_name} build Error")

    def package(self, target_folder):
        """Package the package into a zip and save zip in target directory

        :param target_folder:
        :return:
        """
        utility.make_archive(source=self.lambda_path + "/.aws-sam/build/utilities",
                             destination=target_folder,
                             archive_name=self.package_name
                             )

    def deploy_package_to_s3(self, s3client):
        """Deploy local utilities package to S3 artifact bucket
        :return:
        """
        logger.info("Deploy local utilities package to S3 artifact bucket")
        current_version = self.get_latest_version_number()

        new_version = current_version + 1

        # Deploy the latest build of utilities
        make_archive(source='.build/utilities', destination='.build', archive_name='utilities_latest')

        s3client.upload_file(
            file_name='.build/utilities_latest.zip',
            object_key=f"{constant.DEFAULT_ARTIFACT_PREFIX()}/latest/utilities_{new_version}.zip"
        )

        self.update_version_number(version_number=new_version)

    def update_package(self, s3client, version="latest"):
        print(f"Update the current utilities package in the project with the {version} of the utilities from S3")

        util_latest_version = self.get_latest_version_number()
        util_file_name = f"utilities_{util_latest_version}.zip"

        s3client.get_file(
            object_key=f"{constant.DEFAULT_ARTIFACT_PREFIX()}/latest/{util_file_name}", file_name='utilities.zip')

        shutil.rmtree("utilities")  # Remove existing utilities folder
        loopi = 0
        while os.path.exists("utilities.txt"):  # check if it exists -- loop until not removed
            # print("exists")
            if loopi < 999999999:
                pass
            loopi = loopi + 1

        with ZipFile('utilities.zip', 'r') as zipObj:
            # Extract all the contents of zip file in different directory
            zipObj.extractall('utilities')

        # Remove the utilities.zip - otherwise it will auto included in glue other files path.
        if os.path.isfile('utilities.zip'):
            os.remove('utilities.zip')

        file = 'utilities_version.txt'
        with open(file, 'w') as filetowrite:
            filetowrite.write(f'{util_latest_version}')

    def get_latest_version_number(self):
        logger.info("get_latest_version_number of the utilities package version")

        ssm_client = SystemManagerClient()

        parameter_name = f'/{constant.ENVIRONMENT}/{constant.REGION_NAME}/datalake/utilities_package/latest/version_number'

        try:
            ssm_client._ssm_client.get_parameter(Name=parameter_name)
        except ClientError as e:
            if e.response['Error']['Code'] == 'ParameterNotFound':
                logger.info("Parameter Not found -- add with 0 version")
                ssm_client._ssm_client.put_parameter(Name=parameter_name, Value='0', Type='String')
            else:
                logger.error("Unexpected error: %s" % e)
                raise NameError("Error while trying to get_latest_version_number of the utilities package version from SSM parameter store.")

        ver_num = ssm_client.get_parameter_value(parameter_name)

        logger.info(f"Latest utilities package version: {ver_num}")
        return int(ver_num)

    def update_version_number(self, version_number):

        logger.info(f"update_version_number of the utilities package ({version_number}) in SSM parameter.")

        ssm_client = SystemManagerClient()

        parameter_name = f'/{constant.ENVIRONMENT}/{constant.REGION_NAME}/datalake/utilities_package/latest/version_number'

        try:
            ssm_client._ssm_client.put_parameter(Name=parameter_name, Value=f'{version_number}', Type='String', Overwrite=True)
        except ClientError as e:
            logger.error("Unexpected error: %s" % e)
            raise NameError("Error while trying to update_version_number of the utilities package version in SSM parameter store.")

