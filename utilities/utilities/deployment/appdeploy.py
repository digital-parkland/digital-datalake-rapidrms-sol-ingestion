import importlib
import os
import shutil
import sys
import json
from zipfile import ZipFile

import boto3
from botocore.exceptions import ClientError
from utilities import constant
import utilities.clients.cloudformation as cf
from utilities.clients.cloudformation import CloudFormationClient
from utilities.clients.s3 import S3Client
from utilities.clients.ssm import SystemManagerClient
from utilities.deployment.samdeploy import SAMDeploy
from utilities.logging.logger import logger
from utilities.deployment.artifactdeploy import ArtifactDeploy
from utilities.utility_parameters_and_tags import create_cf_parameter_list, cf_parameter_list_to_list, read_parameters
from utilities import utility
from utilities.utility import make_archive
from utilities.deployment.utilitiespackage import UtilitiesPackage


class TemplateConfig:
    """Light interface to parse the template entry in the deploy_config files
    """

    def __init__(self, template, environment, s3_artifacts_url_prefix):
        self.template_name = template.get('TemplateName')

        if self.template_name is None or self.template_name == 'SampleTemplate':
            self.template_name = None
            return

        self.template_relative_path = template.get('RelativePath')
        self.template_type = template.get('TemplateType', 'cloudformation')
        self.stack_name = template.get('StackName', self.template_name.replace("_", "-"))
        self.use_body = template.get('UseTemplateBody', False)

        self.ignore_env = template.get('IgnoreEnvironment', False)
        self.parameters = template.get('Parameters', [])
        self.capabilities = template.get('Capabilities')

        self.pre_script = template.get('PreScript').replace("/", ".") if template.get('PreScript') else None
        self.post_script = template.get('PostScript').replace("/", ".") if template.get('PostScript') else None

        if isinstance(self.parameters, str):
            # Assume parameters is a file with list of parameters
            # If it's string then relative path to the "json" file is given
            self.parameters = read_parameters(
                parameter_file=f'./{self.parameters}'.replace('{ENVIRONMENT}', environment))

        # Test style of parameters is used - convert to custom style
        if self.parameters and 'ParameterKey' in self.parameters[0]:
            self.parameters = cf_parameter_list_to_list(in_param_list=self.parameters)

        self.tags = template.get('Tags', [])

        self.cfn_path = os.path.join('.', self.template_relative_path, "%s.yaml" % self.template_name)
        self.cfn_url = os.path.join(s3_artifacts_url_prefix, self.template_relative_path,
                                    "%s.yaml" % self.template_name)

        # Final Stack Name
        self.stack_name_env = self.stack_name if self.ignore_env else "{}-{}".format(self.stack_name, environment)


def insert_default_tags(template_config: TemplateConfig):
    """ Insert default parameters

    :param template_config:
    :return:
    """
    tags = template_config.tags
    tag_lst = constant.GLOBAL_TAGS + tags if tags else constant.GLOBAL_TAGS
    lst = utility.create_key_value_list(in_dict_list=tag_lst)

    # Get unique list of parameters
    final_tag_lst = list({v['Key']: v for v in lst}.values())

    return final_tag_lst


class AppDeploy:
    def __init__(self, environment):

        self.environment = environment
        self._cf_client = CloudFormationClient()
        self.update_utilities = True

        try:
            self.aws_account = boto3.client('sts', region_name=constant.REGION_NAME).get_caller_identity()['Account']
            logger.info("AWS account:", self.aws_account)

            self.artifact_deploy = ArtifactDeploy(environment=environment)

            # # #### Below should NOT run from the "digital-infrastructure" project
            if self.update_utilities and self.artifact_deploy.application_name != constant.UTILITIES_APP_NAME:
                # Download utilities
                util_pac = UtilitiesPackage()
                util_pac.update_package(s3client=self.artifact_deploy.s3client)

            # Build with latest utilities
            self.artifact_deploy.build()

        except ClientError as ex:
            logger.error(ex)
            sys.exit(1)

    def deploy_artifacts(self):
        """Deploys artifacts to S3 artifacts bucket

        :return:
        """
        self.artifact_deploy.deploy()  # Deploy artifacts to artifact bucket

        # #### Below should only run from the "digital-infrastructure" code
        if self.artifact_deploy.application_name == constant.UTILITIES_APP_NAME:
            # Deploy the latest build of utilities
            util_pac = UtilitiesPackage()
            util_pac.deploy_package_to_s3(s3client=self.artifact_deploy.s3client)

    def deploy(self, stack_name=None):
        """Deploy CloudFormation

        :param stack_name: If None then deploy all stacks, else only deploy this stack using template body Not through
        :return:
        """
        template_config_file = constant.TEMPLATES_CONFIG_FILE
        try:
            template_configs = json.load(open(template_config_file, 'r'))['Templates']
        except FileNotFoundError as ex:
            logger.error(ex)
            sys.exit(1)

        # For each template config in the file deploy the object.
        for t_config in template_configs:

            template_config = TemplateConfig(template=t_config, environment=self.environment,
                                             s3_artifacts_url_prefix=self.artifact_deploy.s3_artifact_url)

            if template_config is None or template_config.template_name is None:
                continue  # Config is empty ignore entry

            # ### Insert default parameters & Tags
            template_config.parameters = self.insert_default_parameters(template_config=template_config)
            template_config.tags = insert_default_tags(template_config=template_config)

            # Run pre script
            if template_config.pre_script:
                logger.info(self.__class__.__name__, 'pre script', template_config.template_name)
                pre_module = importlib.import_module(template_config.pre_script)
                pre_module.main(template_config)

            logger.info(self.__class__.__name__, 'deploy', template_config.template_name)
            if stack_name is None and template_config.stack_name in [constant.KMS_TEMPLATE_STACK_NAME,
                                                                     constant.S3_TEMPLATE_STACK_NAME,
                                                                     'sample-stack-name']:
                # Ignore Sample, KMS and S3 templates - unless directly called
                continue
            elif stack_name and stack_name != template_config.stack_name:
                # stack_name is set and current config then continue
                continue
            elif template_config.template_type in ['sam', 'lambda']:
                self.deploy_sam(template_config=template_config)
                self.run_post_script(template_config=template_config)
                if stack_name:
                    return
            elif template_config.template_type == 'glue':
                self.deploy_glue(template_config=template_config)
                self.run_post_script(template_config=template_config)
                if stack_name:
                    return
            else:
                self.deploy_cloudformation(template_config=template_config)
                self.run_post_script(template_config=template_config)
                if stack_name:
                    return

    def run_post_script(self, template_config):

        if template_config.post_script:
            logger.info(self.__class__.__name__, 'post script', template_config.template_name, ' Stack Name: ',
                        template_config.stack_name_env)
            post_module = importlib.import_module(template_config.post_script)
            post_module.main(template_config)

    def deploy_sam(self, template_config):
        """Deploy lambda using sam

        :template: Template json string from deploy_config.json
        :return:
        """
        lambda_path = os.path.join(template_config.template_relative_path, template_config.template_name)

        sam_deploy = SAMDeploy(lambda_dir=lambda_path)
        sam_deploy.build_lambda(stack_name=template_config.stack_name_env)
        sam_deploy.deploy_lambda(stack_name=template_config.stack_name_env,
                                 s3_artifact_bucket=self.artifact_deploy.s3_artifact_bucket_name,
                                 s3_artifact_prefix=self.artifact_deploy.s3_artifact_prefix,
                                 parameters=template_config.parameters,
                                 tags=template_config.tags,
                                 capabilities=template_config.capabilities)

    def deploy_glue(self, template_config):
        """Deploy glue job

        :param template_config:
        :return:
        """

        glue_job_name = cf.get_parameter_value_from_param_list(key_name='pGlueJobName',
                                                               parameter_list=template_config.parameters)

        ########################################################################################
        # Create default log groups
        utility.create_log_group("/aws-glue/jobs/logs-v2")  # Log
        utility.create_log_group("/aws-glue/jobs/error")  # Error
        utility.create_log_group("/aws-glue/jobs/output")  # Output

        # When Glue Job and glue role and GlueSecurityConfig defined for the job
        utility.create_log_group(f"/aws-glue/jobs/logs-v2-{glue_job_name}-{self.environment}")  # Log
        utility.create_log_group(
            f"/aws-glue/jobs/{glue_job_name}-{self.environment}-pki-{glue_job_name}-RunRole-{self.environment}/error")  # Error
        utility.create_log_group(
            f"/aws-glue/jobs/{glue_job_name}-{self.environment}-pki-{glue_job_name}-RunRole-{self.environment}/output")  # Output

        ########################################################################################
        # Temp prefix

        ssm_client = SystemManagerClient()
        temp_s3 = ssm_client.get_parameter_value(
            f'/{constant.ENVIRONMENT}/{constant.REGION_NAME}/datalake/s3/temp/name')

        s3_client = S3Client(url=f's3://{temp_s3}')
        s3_client.check_and_create_prefix(prefix=glue_job_name)

        ########################################################################################
        # Deploy
        logger.info(self.__class__.__name__, "deploy glue job", glue_job_name, ' Stack Name: ',
                    template_config.stack_name_env)

        template_config.cfn_path = os.path.join('.', template_config.template_relative_path,
                                                template_config.template_name,
                                                "%s.yaml" % template_config.template_name)
        template_config.cfn_url = os.path.join(self.artifact_deploy.s3_artifact_url,
                                               template_config.template_relative_path, template_config.template_name,
                                               "%s.yaml" % template_config.template_name)

        # utility.create_kv_dict_list(
        # Tags to glue job are not applied as expected
        # Need to manually add the tags
        #   https://docs.aws.amazon.com/glue/latest/dg/monitor-tags.html
        #     Tags need to be in form {'key1': 'v1', 'k2': 'v2', ...}
        # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glue.html#Glue.Client.tag_resource
        #
        #
        # -- issue https://github.com/aws-cloudformation/aws-cloudformation-coverage-roadmap/issues/306
        #
        self._cf_client.deploy_stack(stack_name=template_config.stack_name_env,
                                     template_url=template_config.cfn_url, parameters=template_config.parameters,
                                     tags=template_config.tags, capabilities=template_config.capabilities)

        # TODO: Add tags manually to the glue job
        #  >> Show this in the post script

    def deploy_cloudformation(self, template_config):
        """Deploy single template

        :template: Template json string from deploy_config.json
        :return:
        """

        if template_config.use_body:
            # If S3 Buckets have NOT been deployed then deploy from local
            template_body = open(template_config.cfn_path, 'r').read()

            self._cf_client.deploy_stack(stack_name=template_config.stack_name_env,
                                         template_body=template_body, parameters=template_config.parameters,
                                         tags=template_config.tags, capabilities=template_config.capabilities)
        else:
            # Deploy from S3
            self._cf_client.deploy_stack(stack_name=template_config.stack_name_env,
                                         template_url=template_config.cfn_url, parameters=template_config.parameters,
                                         tags=template_config.tags, capabilities=template_config.capabilities)

    def insert_default_parameters(self, template_config: TemplateConfig):
        """ Insert default parameters

        :param template_config:
        :return:
        """

        parameters = template_config.parameters

        # Test style of parameters is used -- Don't need to do this here -- it's done in the Temp Config class

        if template_config.template_relative_path is None:
            template_config.template_relative_path = 'src'

        auto_inject_params = [
            {'pArtifactS3Prefix': f'{self.artifact_deploy.s3_artifact_prefix}'},
            # Base prefix where artifacts of this project will be uploaded to.
            {'pArtifactRelativePath': f'{template_config.template_relative_path}'},
            {'pTemplateName': f'{template_config.template_name}'},
        ]

        if template_config.template_type == 'glue':
            auto_inject_params.extend(
                [
                    # {'pGlueJobName': f'{template_config.template_name}'},     template - name has word "glue" in it
                    {
                        'pArtifactJobCodePath': f'{self.artifact_deploy.s3_artifact_prefix}/{template_config.template_relative_path}/{template_config.template_name}/job/job.py'.replace(
                            '//', '/')},
                    {'pExtraPyFiles': ",".join(self.artifact_deploy.zip_files_list)},
                    {'pExtraJars': ",".join(self.artifact_deploy.jar_files_list)},

                ]
            )

        elif template_config.template_type == 'sam':
            print("No additional sam injected parameters")
            # auto_inject_params.append(
            #     {'pLambdaName': f'{template_config.template_name}'},
            # ) ParameterKey

        # Get unique list of parameters
        lst = create_cf_parameter_list(
            in_param_list=(constant.GLOBAL_CF_PARAMETERS() + auto_inject_params + parameters))

        final_parm_list = list({v['ParameterKey']: v for v in lst}.values())

        return final_parm_list
