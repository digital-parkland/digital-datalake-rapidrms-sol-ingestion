""" AWS Lambda deployment using SAM

Revisions:
    2020-09-19 San Brar    Initial version
"""
import subprocess
from utilities.logging.logger import logger
import utilities.constant as const
from utilities import utility
import os
from utilities.utility_parameters_and_tags import create_cf_parameter_list, cf_parameter_list_to_list, \
    read_parameters


class SAMDeploy:
    """ Leverage SAM to deploy lambda

    Attributes:
        lambda_dir: str
                    lambda directory must contain the following:
                     a) app.py          >>> lambda_handler
                     b) template.yaml   >>> SAM CloudFormation
                     c) requirements.txt
    """

    def __init__(self, lambda_dir):
        """Initialize class"""
        self.lambda_dir = lambda_dir

    def build_lambda(self, stack_name):
        """Build lambda package using SAM CLI


        :param stack_name:
        :return:
        """
        logger.info(self.__class__.__name__, "SAM - AppDeploy", stack_name)

        try:
            build_p = subprocess.run(['sam', 'build'], cwd=self.lambda_dir, capture_output=True, text=True)

            logger.info(self.__class__.__name__, build_p.stdout, stack_name)

            if build_p.returncode != 0:
                logger.error(self.__class__.__name__, build_p.stderr, stack_name)
                raise Exception("Build Error")

        except Exception as ex:
            logger.error(self.__class__.__name__, str(ex))
            raise

    def deploy_lambda(self, stack_name, s3_artifact_bucket, s3_artifact_prefix, parameters=None, capabilities=None,
                      tags=None):
        """Package and deploy lambda package using SAM CLI
            Process builds samconfig.toml file to deploy.

        :param stack_name: Stack Name
        :param s3_artifact_bucket:
        :param s3_artifact_prefix:
        :param parameters: dictionary of parameter_file {"var1_name": "var1_value", "var2_name": "var2_value", ....}
        :param capabilities:
        :param tags: dictionary of tags {"tag1_name": "tag1_value", "tag2_name": "tag2_value", ....}
        :return:
        """

        # ###### Build the config file ######
        logger.info(self.__class__.__name__, "SAM - create samconfig.toml", stack_name)

        try:

            capab = [] if capabilities is None else capabilities
            sam_config_file_name = f"{self.lambda_dir}/samconfig.toml"

            with open(sam_config_file_name, 'w') as f:
                f.write("version = 0.1\n")
                f.write("[default.deploy.parameters]\n")
                f.write(f'stack_name = "{stack_name}"\n')
                f.write(f's3_bucket = "{s3_artifact_bucket}"\n')
                f.write(f's3_prefix = "{(s3_artifact_prefix + "/" + self.lambda_dir).replace("//", "/")}"\n')
                f.write(f'region = "{const.REGION_NAME}"\n')
                f.write(f'confirm_changeset = false\n')
                f.write(f'capabilities = "{capab}"\n')
                f.write(f'parameter_overrides = {create_samconfig_string(input_list=parameters)}\n')
                f.write(f'tags = {create_samconfig_string(input_list=tags)}\n')

            logger.info(self.__class__.__name__, "SAM - deploy", stack_name)

            # files = utility.os_get_only_files(my_path=self.lambda_dir + "/.aws-sam/build")
            # # Remove the "gluesparkjobutil.py" file # Not compatible with lambda
            #
            # matches = [x for x in files if str(x).endswith('gluesparkjobutil.py')]
            # for f in matches:
            #     os.remove(f)
            #
            deploy_p = subprocess.run(['sam', 'deploy'], cwd=self.lambda_dir, capture_output=True, text=True)

            logger.info(self.__class__.__name__, deploy_p.stdout, stack_name)

            if deploy_p.returncode != 0:
                if "No changes to deploy" in str(deploy_p.stderr):
                    logger.info(self.__class__.__name__, deploy_p.stderr, stack_name)
                else:
                    logger.error(self.__class__.__name__, deploy_p.stderr, stack_name)
                    raise Exception("SAM Deploy Error")

        except Exception as ex:
            logger.error(self.__class__.__name__, str(ex))
            raise


def create_samconfig_string(input_list):
    """
    Functions create tag string which is applied to all resources in sam template
    :return:
    """
    s_out = ""
    for a in input_list:
        if 'ParameterKey' in a:
            s_out = s_out + f"{a.get('ParameterKey')}=\\\"{a.get('ParameterValue')}\\\" "
        elif 'Key' in a:
            s_out = s_out + f"{a.get('Key')}=\\\"{a.get('Value')}\\\" "
        else:
            logger.error(__name__, 'Parameter or tag list has in correct format')
            raise Exception('create_samconfig_string - Parameter or tag list has in correct format')

    # for a in iter(input_dic.items()):
    #     s_out = s_out + f"{a[0]}=\\\"{a[1]}\\\" "

    return f'"{s_out}"'

# def create_samconfig_string1(keys_and_values: list):
#     """
#     Functions create tag string which is applied to all resources in sam template
#     :return:
#     """
#     s_out = ""
#     for t in keys_and_values:
#         s_out = s_out + f"{t['Key']}=\\\"{t['Value']}\\\" "
#
#     return f'"{s_out}"'
