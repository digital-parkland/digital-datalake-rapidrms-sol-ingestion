"""AWS DMS interface

Revisions:
    2020-09-19 San Brar    Initial version

"""
##########################################################################
# Copyright 2020-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
# http://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the
# License for the specific language governing permissions and limitations under the License.
##########################################################################
import json
import shutil
from utilities.clients.s3 import S3Client
import os
import os.path
from os import path
from utilities.logging.logger import logger
from utilities.deployment.utilitiespackage import UtilitiesPackage
from utilities import utility
import utilities.constant as const


def get_deployment_information():
    """Get deployment information from _deploy.json file

    :return:
        app_name
        deployment_version
        error_message
    """
    # Check if file exists
    deploy_file_exists = path.exists(const.ARTIFACT_DEPLOY_CONFIG_FILE_NAME)

    if deploy_file_exists:
        # Get app name and deployment version from deploy file
        f = open(const.ARTIFACT_DEPLOY_CONFIG_FILE_NAME, "r")
        file: dict = json.load(f)

        if ('app_name' in file) and ('deployment_version' in file):
            return file['app_name'].lower(), file['deployment_version'].lower(), None
        else:
            return None, None, f"Missing keys (app_name, deployment_version) in file {const.ARTIFACT_DEPLOY_CONFIG_FILE_NAME}"
    else:
        error_msg = f"Deployment file: {const.ARTIFACT_DEPLOY_CONFIG_FILE_NAME} not found"
        return None, None, error_msg


def get_version_information(environment):
    """Get current version information from _version.json file

    :return:
        version
        error_msg
    """
    # Check if file exists

    version_file_exists = path.exists(const.ARTIFACT_VERSION_CONFIG_FILE_NAME)

    if version_file_exists:
        # Get application version
        f = open(const.ARTIFACT_VERSION_CONFIG_FILE_NAME, "r")
        file: dict = json.load(f)

        if not environment in file:
            return None, f"Missing configuration for environment {environment} in {const.ARTIFACT_VERSION_CONFIG_FILE_NAME}"

        if 'version' in file[environment]:
            app_v = file[environment]['version'].lower()
            print('========= Application Version:', app_v)
            return app_v, None
        else:
            return None, f"Missing key (version) for environment {environment} in file {const.ARTIFACT_VERSION_CONFIG_FILE_NAME}"
    else:
        error_msg = f"Version file: {const.ARTIFACT_VERSION_CONFIG_FILE_NAME} not found"

        return None, error_msg


class ArtifactDeploy:
    """ Deploy artifact (glue and lambda) to S3 artifact bucket
            Leverages "_deploy.json" and "_version.json"files

    Deployment for following are supported:
      a) glue
      b) lambda - leverages SAM
      c) dms
    """
    s3_artifact_prefix: str

    def __init__(self, environment, artifact_bucket=None):
        self.build_folder = ".build"

        self.environment = environment if environment else const.ENVIRONMENT
        utility.set_environmet_variable(env_var_name="ENVIRONMENT", env_var_value=self.environment)

        self.s3_artifact_bucket_name = artifact_bucket if artifact_bucket else const.DEFAULT_ARTIFACT_S3_BUCKET()

        # Get info from deployment and version files
        self.application_name, self.deployment_version, deploy_file_error = get_deployment_information()
        self.app_version, version_file_error = get_version_information(self.environment)

        if deploy_file_error or version_file_error:
            logger.error(self.__class__.__name__, deploy_file_error)
            logger.error(self.__class__.__name__, version_file_error)
            raise Exception(f"{deploy_file_error} \n {version_file_error}")

        self.s3_artifact_prefix = f'{const.DEFAULT_ARTIFACT_PREFIX()}/{self.application_name}/{self.app_version if self.deployment_version == "latest" else self.deployment_version}'
        self.s3_artifact_url = f"https://{self.s3_artifact_bucket_name}.s3.amazonaws.com/{self.s3_artifact_prefix}"
        self.s3_artifact_path = f's3://{self.s3_artifact_bucket_name}/{self.s3_artifact_prefix}'
        self.s3client = S3Client(url=self.s3_artifact_url)

        self.build_file_list = []
        self.python_files_list = []
        self.jar_files_list = []
        self.zip_files_list = []
        self.resources_files_list = []

        logger.info(self.__class__.__name__, "__init__", self.application_name)

    def build(self):
        """ Build current package and utilities package if present in project.
                Put the URL together
                    - Put together list of items needed.
         For glue
          > libary  (list of py files or python packaged ziped)
                       src will be ziped to the library folder
          > jars    (list of dependent jars)
          > referenced
          > CloudFormation
          > glue
        """
        # Build application
        if self.deployment_version == 'latest':
            logger.info(self.__class__.__name__, "Build the latest version of the application", self.application_name)

            try:
                # Copy files to build folder
                if os.path.exists(self.build_folder):
                    shutil.rmtree(self.build_folder)

                while os.path.exists(self.build_folder):  # check if it exists
                    pass

                # Move all files to build directory
                shutil.copytree(src=os.getcwd(), dst=self.build_folder,
                                ignore=shutil.ignore_patterns(*const.ARTIFACT_BUILD_IGNORE_LIST))

                # TODO: Utilities package may not always be in the project.
                utils_package = UtilitiesPackage()
                utils_package.build()
                utils_package.package(target_folder=self.build_folder + "/resources")

                # Build file list
                self.build_file_lists()

                logger.info(self.__class__.__name__,
                            f"Build completed see '{self.build_folder}' directory",
                            self.application_name)

            except Exception as ex:
                logger.error(self.__class__.__name__, ex)
                raise
            else:
                return True

        else:
            logger.info(self.__class__.__name__,
                        f"Build NOT needed because request is to deploy an existing version: {self.deployment_version}",
                        self.application_name)

    def deploy(self):
        """Deploy objects in the build directory to the artifact bucket

        """

        if self.deployment_version == 'latest':
            self.s3client.check_and_create_prefix(prefix=self.s3_artifact_prefix, create_prefix=True)
            logger.info(self.__class__.__name__, f"Uploading artifact in S3", self.application_name)
            for file_name in self.build_file_list:
                self.s3client.upload_file(file_name=f'{self.build_folder}/{file_name}', object_key=f'{self.s3_artifact_prefix}/{file_name}')
        else:
            logger.info(self.__class__.__name__, f"No artifacts deployed to {self.s3_artifact_bucket_name}",
                        self.application_name)

    def build_file_lists(self):
        """Builds file lists for src and jar files
        """
        # Get list of files in src folders

        files = utility.os_get_only_files(my_path=self.build_folder)
        for file in files:
            file_cleaned = file.replace(self.build_folder + "/", "")
            file_s3_path = f'{self.s3_artifact_path}/{file_cleaned}'

            self.build_file_list.append(file_cleaned)

            if file_cleaned.endswith('.jar'):
                self.jar_files_list.append(file_s3_path)
            elif file_cleaned.endswith('.zip'):
                self.zip_files_list.append(file_s3_path)
            elif file_cleaned.endswith('.py'):
                self.python_files_list.append(file_s3_path)
            if file_cleaned.startswith('resources/'):
                self.resources_files_list.append(file_s3_path)

        return None

    # def upload_file(self, file_name, bucket, object_key=None):
    #     """Upload a file to an S3 bucket
    #     :param file_name: File to upload
    #     :param bucket: Bucket to upload to
    #     :param object_key: S3 object name. If not specified then file_name is used
    #     :return: True if file was uploaded, else False
    #     """
    #     # If S3 object_key was not specified, use file_name
    #     if object_key is None:
    #         object_key = file_name
    #
    #     # Upload the file
    #     self.logger.info(f"==== Upload file {file_name} to {bucket} with key {object_key} ====")
    #     try:
    #         response = self.s3client.upload_file(file_name, bucket, object_key)
    #     except ClientError as e:
    #         logging.error(e)
    #         return False
    #     return True
