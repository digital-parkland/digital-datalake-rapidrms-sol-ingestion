import pkgutil


def custom_loader(module_name):
    try:
        # in case of a reload
        return loader.find_module(module_name).load_module(module_name)
    except Exception as ex:
        print(ex)
        if ex.name == 'awsglue':
            pass
        elif ex.name == 'pyspark':
            pass


__all__ = []
for loader, module_name, is_pkg in pkgutil.walk_packages(__path__):

    if module_name == 'etlops.gluesparkjobutil' or module_name == 'logging.gluelogger':
        __all__.append(module_name)
        _module = custom_loader(module_name)
        globals()[module_name] = _module
    else:
        __all__.append(module_name)
        _module = loader.find_module(module_name).load_module(module_name)
        globals()[module_name] = _module
