import boto3
import utilities.constant as const


class SnsClient:
    """
    Attributes:
        parameter (str): AWS System Manager parameter name
        parameter_value (str): AWS System Manager parameter value
    """

    def __init__(self, sns_arn):
        """Initialize class"""
        self._sns_client = boto3.client('sns', region_name=const.REGION_NAME)
        self.sns_arn = sns_arn

    def publish(self, msg):
        # ## Send notification

        self._sns_client.publish(Message=msg, TopicArn=self.sns_arn)
