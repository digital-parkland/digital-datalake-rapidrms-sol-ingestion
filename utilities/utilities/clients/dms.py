"""AWS DMS interface

Revisions:
    2020-09-19 San Brar    Initial version

"""

import boto3
from botocore.exceptions import ClientError
import utilities.constant as const
from utilities.logging.logger import logger


class DMSClient:
    """
    Attributes:
        parameter (str): AWS System Manager parameter name
        parameter_value (str): AWS System Manager parameter value
    """

    def __init__(self):
        """Initialize class"""
        self._dms_client = boto3.client('dms', region_name=const.REGION_NAME)
        self.parameter = None
        self.parameter_value = None

    def get_parameter_value(self, parameter):
        """Get the value of the parameter
        Args:
            parameter (str): AWS System Manager parameter name
        Returns:
            str:
        Raises:
            ValueError, ClientError
        """
        self.parameter = parameter
        try:
            logger.info(self.__class__.__name__, "parameter", self.parameter)
            response = self._dms_client.get_parameter(Name=self.parameter)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            self.parameter_value = response['Parameter']['Value']
            logger.info(self.__class__.__name__, "value", self.parameter_value)
            return self.parameter_value
