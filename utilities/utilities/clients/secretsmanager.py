"""Secrets Manager interface

Revisions:
    2020-09-19 San Brar    Initial version

"""

import json
import boto3
from botocore.exceptions import ClientError
import utilities.constant as const
from utilities.logging.logger import logger


class SecretsManagerClient:
    """
    Attributes:
        secretid (str): AWS Secrets Manager secret id
        secret_json (str): Secret string in JSON format.
            Binary secret is not supported
    """

    def __init__(self):
        """Initialize class"""
        self._session = boto3.session.Session()
        self._asm_client = self._session.client('secretsmanager', region_name=const.REGION_NAME)
        self.secretid = None
        self.secret_json = None

    def get_secret_json(self, secretid):
        """Get the secret string of the secret id
        Args:
            secretid (str): AWS Secrets Manager secret id
        Returns:
            dict: Secret string
        Raises:
            ValueError, ClientError
        """
        self.secretid = secretid
        try:
            logger.info(self.__class__.__name__, "get", self.secretid)
            secret_value = self._asm_client.get_secret_value(SecretId=self.secretid)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            if 'SecretString' in secret_value:
                self.secret_json = json.loads(secret_value['SecretString'])
            else:
                # NOTE: binary secret is not supported
                #self.secret_binary = base64.b64decode(secret_value['SecretBinary'])
                err_msg = "SecretString not found"
                logger.error(self.__class__.__name__, err_msg)
                raise ValueError(err_msg)
        return self.secret_json
