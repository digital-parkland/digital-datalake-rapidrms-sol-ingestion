"""AWS DynamoDB interface

Revisions:
    2020-09-19 San Brar    Initial version

"""

import boto3
import os
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
from utilities.logging.logger import logger
import utilities.constant as const


class DynamoDBClient:
    """
    Attributes:
        None
    """

    def __init__(self):
        """Initialize class"""
        self._dynamodb = boto3.resource('dynamodb', region_name=const.REGION_NAME)

    def scan(self, table, filter_on, condition, pe=None):
        """Scan table filter on condition and return array of projection expression
        Args:
            table (str): ddb table name
            filter_on (str):
            condition (str):
            pe (str): Optional, default None
        Returns:
            list: result
        Raise:
            ClientError
        """
        ddb_table = self._dynamodb.Table(table)
        try:
            logger.info(self.__class__.__name__, "scan", table, filter_on, condition, pe)
            fe = Attr(filter_on).eq(condition)
            if pe is None:
                response = ddb_table.scan(FilterExpression=fe)
            else:
                response = ddb_table.scan(FilterExpression=fe, ProjectionExpression=pe)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return None if response['Items'] == [] else response['Items']

    def query(self, table, key, condition, pe=None):
        """Query table by key and return array of projection expression
        Args:
            table (str): ddb table name
            key (str):
            condition (str):
            pe (str): Optional, default None
        Returns:
            list: result
        Raise:
            ClientError
        """
        # TODO: support index
        ddb_table = self._dynamodb.Table(table)
        try:
            logger.info(self.__class__.__name__, "query", table, key, condition, pe)
            ke = Key(key).eq(condition)
            if pe is None:
                response = ddb_table.query(KeyConditionExpression=ke)
            else:
                response = ddb_table.query(KeyConditionExpression=ke, ProjectionExpression=pe)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return None if response['Items'] == [] else response['Items']
