"""AWS CloudFormation interface

Revisions:
    2020-09-19 San Brar    Initial version

"""
import time
import boto3
from botocore.exceptions import ClientError, WaiterError
import utilities.constant as const
from utilities.logging.logger import logger


def get_export_output(output_key):
    """Get CloudFormation export output

    :param output_key:
    :return:
    """
    cf = _cf_client = boto3.client('cloudformation', region_name=const.REGION_NAME)
    logger.info(__name__, f"get cloudformation export by key {output_key} ======")
    try:

        response = cf.list_exports()

        exports = response.get('Exports')
        if not exports:
            logger.info(__name__, f"Export key '{output_key}' not found ======")
            return None

        for i in exports:
            if i.get('Name') == output_key:
                return i.get('Value')

        next_token = response.get('NextToken')

        while next_token:
            response = cf.list_exports(NextToken=next_token)
            exports = response.get('Exports')
            for i in exports:
                if i.get('Name') == output_key:
                    return i.get('Value')

            next_token = response.get('NextToken')

    except ClientError as ex:
        logger.error(__name__, ex)
        raise


def get_parameter_value_from_param_list(key_name, parameter_list):
    """Return value of the parameter. Searches the parameter_list

    :param parameter_list:
    :param key_name:
    :return:
    """

    for d in parameter_list:
        if 'ParameterKey' in d and d.get('ParameterKey') == key_name:
            return d.get('ParameterValue')
        elif key_name in d:
            return d.get(key_name)

    return None


def set_parameter_value_in_param_list(key_name, parameter_list, parameter_value):
    """Return value of the parameter. Searches the parameter_list

    :param parameter_value:
    :param parameter_list:
    :param key_name:
    :return:
    """

    for d in parameter_list:
        if 'ParameterKey' in d and d.get('ParameterKey') == key_name:
            d['ParameterValue'] = parameter_value
        elif key_name in d:
            d[key_name] = parameter_value

    return None


class CloudFormationClient:
    """
    Attributes:
        custom_waiter: Use custom waiter. Default True.
    """

    def __init__(self, custom_waiter=True):
        """Initialize class"""
        self._cf_client = boto3.client('cloudformation', region_name=const.REGION_NAME)
        self.custom_waiter = custom_waiter

    def check_stack_exists(self, stack_name):
        """Check stack exists and status
        Args:
            stack_name (str):
        Returns:
            boolean: stack exists
            str: stack status
        """
        try:
            stack = self._cf_client.describe_stacks(StackName=stack_name)['Stacks'][0]
            return stack['StackName'] == stack_name, stack['StackStatus']
        except ClientError:
            return False, None

    def delete_stack(self, stack_name):
        """Delete stack
        Args:
            stack_name (str):
        Returns:
            None
        Raise:
            ClientError, WaiterError
        """
        try:
            logger.info(self.__class__.__name__, "delete", stack_name)
            self._cf_client.delete_stack(StackName=stack_name)
            waiter = CFWaiter(cf_client=self._cf_client, waiter_name=const.CF_DELETE_STACK_WAITER_NAME,
                              custom_waiter=self.custom_waiter)
            waiter.wait(stack_name=stack_name)

        except (ClientError, WaiterError) as ex:
            logger.error(self.__class__.__name__, ex)
            raise

    def deploy_stack(self, stack_name, template_url=None, template_body=None, parameters=None, capabilities=None,
                     tags: list = None):
        """ Create or update stack

        :param stack_name: Name
        :param template_url: S3 URL
        :param template_body: Stack Body
        :param parameters: Stack Input Parameters
        :param capabilities: Stack Capabilities
        :param tags: Stack Tags
        :returns: None
        Raise:
            ClientError, WaiterError, ValueError
        """

        stack_exists, stack_status = self.check_stack_exists(stack_name)

        capab = [] if capabilities is None else capabilities

        if template_url is None and template_body is None:
            message = "Template url and Template body can not both be empty"
            logger.error(self.__class__.__name__, message)
            raise ValueError(message)

        try:
            # Validate stack
            if template_body:
                self._cf_client.validate_template(TemplateBody=template_body)
            else:
                self._cf_client.validate_template(TemplateURL=template_url)

            # Create or update stack
            if stack_exists:
                logger.info(self.__class__.__name__, "update", stack_name)

                if template_body:
                    self._cf_client.update_stack(
                        StackName=stack_name, TemplateBody=template_body, Parameters=parameters, Capabilities=capab,
                        Tags=tags)
                else:
                    self._cf_client.update_stack(
                        StackName=stack_name, TemplateURL=template_url, Parameters=parameters, Capabilities=capab,
                        Tags=tags)

                waiter = CFWaiter(cf_client=self._cf_client, waiter_name=const.CF_UPDATE_STACK_WATIER_NAME,
                                  custom_waiter=self.custom_waiter)

            else:
                logger.info(self.__class__.__name__, "create", stack_name)
                if template_body:
                    self._cf_client.create_stack(
                        StackName=stack_name, TemplateBody=template_body, Parameters=parameters, Capabilities=capab,
                        Tags=tags)
                else:
                    self._cf_client.create_stack(
                        StackName=stack_name, TemplateURL=template_url, Parameters=parameters, Capabilities=capab,
                        Tags=tags)

                waiter = CFWaiter(cf_client=self._cf_client, waiter_name=const.CF_CREATE_STACK_WAITER_NAME,
                                  custom_waiter=self.custom_waiter)

            logger.info(self.__class__.__name__, "wait", stack_name)
            waiter.wait(stack_name=stack_name)
            logger.info(self.__class__.__name__, "complete", stack_name)

        except ClientError as ex:
            error_message = ex.response['Error']['Message']
            if error_message == const.CF_NO_UPDATE_ERROR:
                logger.info(self.__class__.__name__, ex)
            else:
                logger.error(self.__class__.__name__, ex)
                raise
        except WaiterError as ex:
            logger.error(self.__class__.__name__, ex)
            raise


class CFWaiter:

    def __init__(self, cf_client, waiter_name, custom_waiter=False, delete_stack_on_failure=True):
        """Initialize class"""
        self._cf_client = cf_client
        self.waiter_name = waiter_name
        self.custom_waiter = custom_waiter
        self.delete_stack_on_failure = delete_stack_on_failure

    def wait(self, stack_name):
        """ Wait
        Polls CloudFormation.Client.describe_stacks() every x seconds until a successful state is reached.
        An error is returned after y failed checks.

        :param stack_name:
        :return:
        """
        try:

            if self.custom_waiter:
                return self.waiter(stack_name=stack_name)
            else:
                waiter = self._cf_client.get_waiter(waiter_name=self.waiter_name)
                waiter.wait(
                    StackName=stack_name,
                    WaiterConfig={
                        'Delay': const.CF_WAITER_DELAY,
                        'MaxAttempts': const.CF_WAITER_MAX_ATTEMPTS
                    }
                )

                return 'Success', None

        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        except WaiterError as ex:
            logger.error(self.__class__.__name__, ex)
            raise

    def waiter(self, stack_name):
        """Custom waiter - print stack events to logger

        :param stack_name:
        :return:
        """

        logger.info(self.__class__.__name__, "custom waiter", stack_name)

        try:
            # Initialization
            stack_status = "Initial-Wait"
            attempt = 0
            success_lst = []
            failure_lst = []
            event_ids = []

            # Set success and failure list
            if self.waiter_name == const.CF_UPDATE_STACK_WATIER_NAME:
                success_lst = const.CF_UPDATE_STACK_SUCCESS_LIST
                failure_lst = const.CF_UPDATE_STACK_FAILURE_LIST

            elif self.waiter_name == const.CF_CREATE_STACK_WAITER_NAME:
                success_lst = const.CF_CREATE_STACK_SUCCESS_LIST
                failure_lst = const.CF_CREATE_STACK_FAILURE_LIST

            elif self.waiter_name == const.CF_DELETE_STACK_WAITER_NAME:
                success_lst = const.CF_DELETE_STACK_SUCCESS_LIST
                failure_lst = const.CF_DELETE_STACK_FAILURE_LIST

            # Loop until desired status
            while stack_status not in success_lst:
                time.sleep(const.CF_WAITER_DELAY)

                response = self._cf_client.describe_stacks(StackName=stack_name)
                stack_status = response['Stacks'][0]['StackStatus']
                logger.info(self.__class__.__name__, f"Stack Status: {stack_status}", stack_name)

                if stack_status in failure_lst:
                    logger.error(self.__class__.__name__, "Stack status in failure list", stack_name)

                    if self.delete_stack_on_failure and self.waiter_name == const.CF_CREATE_STACK_WAITER_NAME:
                        logger.info(self.__class__.__name__, "delete", stack_name)
                        self._cf_client.delete_stack(StackName=stack_name)
                        waiter = self._cf_client.get_waiter(waiter_name=const.CF_DELETE_STACK_WAITER_NAME)
                        waiter.wait(StackName=stack_name)
                        response = self._cf_client.describe_stacks(StackName=stack_name)
                        stack_status = response['Stacks'][0]['StackStatus']

                    raise Exception(f"Stack update or create failed: Stack status: {stack_status}")

                event_ids = log_stack_events(cf_client=self._cf_client, stack_name=stack_name, event_ids=event_ids)
                attempt += 1
                if attempt >= const.CF_WAITER_MAX_ATTEMPTS:
                    logger.error(self.__class__.__name__, "Stack time out", stack_name)
                    raise Exception("Stack time out")

            logger.info(self.__class__.__name__, "Stack status wait completed - status in success list", stack_name)

        except (ClientError, WaiterError) as ex:
            logger.error(self.__class__.__name__, ex)
            raise


def log_stack_events(cf_client, stack_name, event_ids=None):
    if event_ids is None:
        event_ids = []

    try:
        response = cf_client.describe_stack_events(StackName=stack_name)
        events = response['StackEvents']

        for event in events:
            event_id = event.get("event_id")
            if event_id not in event_ids:
                log = '{0} | {1} | {2} | {3} | {4} '.format(
                    event.get("Timestamp", None).strftime("%d-%b-%y %H:%M:%S").ljust(22),
                    event.get("LogicalResourceId", "N/A").ljust(25),
                    event.get("ResourceType", "N/A").ljust(15),
                    event.get("ResourceStatus", "N/A").ljust(15),
                    event.get("ResourceStatusReason", "Unknown").ljust(120)
                )

                logger.info("log_stack_events", log, stack_name)

            event_ids.append(event_id)

        return event_ids
    except ClientError as ex:
        logger.error("log_stack_events", ex)
        raise
