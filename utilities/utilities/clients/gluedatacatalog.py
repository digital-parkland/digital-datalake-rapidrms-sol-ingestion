"""AWS Glue data catalog interface

Revisions:
    2020-09-19 San Brar    Initial version
"""

from urllib.parse import urlparse
import boto3
from botocore.exceptions import ClientError
import utilities.constant as const
from utilities.logging.logger import logger


class GlueDataCatalog():
    """
    Attributes:
        database, table, location, comment, jobname, jobkey (str):
        partition_key (dict): partition key
        fields (list): table fields
    """

    def __init__(self, database, table, fields, location, partition_key=None, comment=None, jobname=None, jobkey=None):
        """Initialize class
        Raises:
            ClientError
        """
        self._glue_client = boto3.client('glue', region_name=const.REGION_NAME)
        self.database = database
        self.table = table
        self.location = location
        self.fields = fields
        self.partition_key = partition_key
        self.comment = '' if comment is None else comment
        self.jobname = '' if jobname is None else jobname
        self.jobkey = '' if jobkey is None else jobkey
        self._input_format = 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
        self._output_format = 'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
        self._serde_info = {
            'Name': 'ParquetHiveSerDe',
            'SerializationLibrary': 'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe',
            'Parameters': {
                'serialization.format': '1'
            }
        }
        try:
            self.create_database()
        except ClientError as ex:
            logger.error(self.__class__.__name__, "init", ex)
            raise

    def create_database(self):
        """Create database if not exists
        Raises:
            ClientError
        """
        parsed = urlparse(self.location)
        try:
            self._glue_client.get_database(Name=self.database)
        except ClientError as ex:
            if ex.response['Error']['Code'] == 'EntityNotFoundException':
                try:
                    logger.info(self.__class__.__name__, "Create database", self.database)
                    database_input = {
                        'Name': self.database,
                        'Description': "{} data catalog {}".format(const.S3_PREFIX, self.database),
                        'LocationUri': "{}://{}/{}".format(parsed.scheme, parsed.netloc, parsed.path.split('/')[1]),
                        'Parameters': {}
                    }
                    self._glue_client.create_database(DatabaseInput=database_input)
                except ClientError as ex:
                    logger.error(self.__class__.__name__, "create_database", ex)
                    raise
            else:
                logger.error(self.__class__.__name__, "get_database", ex)
                raise

    def table_exists(self):
        """Check table exists
        Returns:
            boolean:
        Raises:
            ClientError
        """
        try:
            self._glue_client.get_table(DatabaseName=self.database, Name=self.table)
            exists = True
        except ClientError as ex:
            logger.warn(self.__class__.__name__, "get_table", ex)
            exists = False
        logger.info(self.__class__.__name__, "Table exists", self.table, exists)
        return exists

    def delete_table(self):
        """Delete table
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "Delete table", self.table)
        try:
            self._glue_client.delete_table(DatabaseName=self.database, Name=self.table)
        except ClientError as ex:
            if ex.response['Error']['Code'] != 'EntityNotFoundException':
                logger.error(self.__class__.__name__, "delete_table", ex)
                raise

    def create_table(self, recreate=False):
        """Create table
        Args:
            recreate (boolean): Force to re-recreate table
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "Create table", recreate, self.table, self.partition_key)
        if self.partition_key is None:
            partition_keys = []
        else:
            partition_keys = [self.partition_key]
        storage_desc = {
            'Columns': self.fields,
            'Location': self.location,
            'InputFormat': self._input_format,
            'OutputFormat': self._output_format,
            'SerdeInfo': self._serde_info
        }
        table_input = {
            'Name': self.table,
            'Description': self.comment,
            'TableType': 'EXTERNAL_TABLE',
            'StorageDescriptor': storage_desc,
            'PartitionKeys': partition_keys,
            'Parameters': {
                'classification': 'parquet',
                'parquet.compression': const.DEFAULT_PARQUET_COMPRESSION,
                'CreatedByJob': self.jobname,
                'CreatedByJobRun': self.jobkey,
                'Version': "0"
            }
        }
        try:
            if recreate:
                self.delete_table()
            self._glue_client.create_table(DatabaseName=self.database, TableInput=table_input)
        except ClientError as ex:
            logger.error(self.__class__.__name__, "create_table", ex)
            raise

    def delete_partition(self, partlist):
        """Delete table
        Args:
            partlist (list):
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "Delete partition", self.table, self.partition_key['Name'], partlist)
        input_list = [{'Values': [v]} for v in partlist]
        batch = const.MAX_GLUE_PARTITION_DELETE
        for i in range(0, len(input_list), batch):
            to_delete = [{k: v[k]} for k, v in zip(['Values'] * batch, input_list[i:i + batch])]
            logger.info(self.__class__.__name__, "Delete partition", self.table, self.partition_key['Name'], "batch",
                        batch,
                        [k['Values'][0] for k in to_delete])
            try:
                self._glue_client.batch_delete_partition(
                    DatabaseName=self.database, TableName=self.table, PartitionsToDelete=to_delete)
            except ClientError as ex:
                logger.error(self.__class__.__name__, "batch_delete_partition", ex, to_delete)
                raise

    def create_partition(self, partlist, recreate=False):
        """Create table
        Args:
            partlist (list):
            recreate (boolean): Force to re-recreate partition
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "Create partition", self.table, self.partition_key['Name'], partlist)
        if recreate:
            try:
                self.delete_partition(partlist)
            except ClientError as ex:
                logger.error(self.__class__.__name__, "delete_partition", ex, recreate)
                raise

        input_list = []
        for partval in partlist:
            partloc = "{}/{}={}".format(self.location, self.partition_key['Name'], partval)
            logger.debug(self.partition_key, partloc)
            input_dict = {
                'Values': [partval],
                'StorageDescriptor': {
                    'Location': partloc,
                    'InputFormat': self._input_format,
                    'OutputFormat': self._output_format,
                    'SerdeInfo': self._serde_info
                }
            }
            input_list.append(input_dict.copy())

        batch = const.MAX_GLUE_PARTITION_CREATE
        for i in range(0, len(input_list), batch):
            to_create = [{j: v[j], k: v[k]} \
                         for j, k, v in zip(['Values'] * batch, ['StorageDescriptor'] * batch, input_list[i:i + batch])]
            logger.info(self.__class__.__name__, "Create partition", self.table, self.partition_key['Name'], "batch",
                        batch,
                        [k['Values'][0] for k in to_create])
            try:
                self._glue_client.batch_create_partition(
                    DatabaseName=self.database, TableName=self.table, PartitionInputList=to_create)
            except ClientError as ex:
                logger.error(self.__class__.__name__, "batch_create_partition", ex, to_create)
                raise
