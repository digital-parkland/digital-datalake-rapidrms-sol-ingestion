"""AWS S3 interface

Revisions:
    2020-09-19 San Brar    Initial version

"""

import boto3
from botocore.exceptions import ClientError
from urllib.parse import urlparse
import utilities.constant as const
from utilities.logging.logger import logger


class S3Client:
    """
    Attributes:
        url (str): S3 object url Example: "https://s3_artifact_bucket_name.s3.amazonaws.com/s3_artifact_prefix"
                                f"https://{self.s3_artifact_bucket_name}.s3.amazonaws.com/{self.s3_artifact_prefix}"
        bucket (str): S3 bucket
        key (str): S3 key
    """

    def __init__(self, url):
        """Initialize class
        Args:
            url (str): S3 object url
        """
        self._parsed_url = urlparse(url, allow_fragments=False)
        self._s3client = boto3.client('s3', region_name=const.REGION_NAME)
        # .resource('s3', region_name=const.REGION_NAME)

    @property
    def bucket(self):
        """bucket property"""
        b = self._parsed_url.netloc.split(".")
        return b[0]

    @property
    def key(self):
        """key property"""
        return self._parsed_url.path.lstrip('/') + '?' + self._parsed_url.query \
            if self._parsed_url.query else self._parsed_url.path.lstrip('/')

    @property
    def url(self):
        """url property"""
        return self._parsed_url.geturl()

    def get_object(self, object_key=None):
        """Get content of the object
        Args:
        Returns:
            str: object content
        Raises:
            ClientError
        """

        if object_key is None:
            object_key = self.key

        logger.info(self.__class__.__name__, "get", self.bucket, object_key)

        # s3object = self._s3client.Object(bucket_name=self.bucket, key=object_key)
        try:
            response = self._s3client.get_object(Bucket=self.bucket, Key=object_key)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return response['Body'].read().decode('utf-8')

    def get_file(self, file_name, object_key=None):
        """Get content of the object
        Args:
        Returns:
            str: object content
        Raises:
            ClientError
        """

        if object_key is None:
            object_key = self.key

        logger.info(self.__class__.__name__, "get_file", self.bucket, object_key)
        try:
            self._s3client.download_file(Bucket=self.bucket, Key=object_key, Filename=file_name)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise

    def put_object(self, buffer):
        """Put content of the object
        Args:
            buffer (str):
        Returns:
            str: S3 put response
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "put", self.bucket, self.key)
        s3object = self._s3client.Object(self.bucket, self.key)
        try:
            response = s3object.put(Body=buffer)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return response

    def list_object(self, prefix=None):
        """List objects of the bucket with prefix filter
        Args:
            prefix (str):
        Returns:
            list: s3.ObjectSummary list
        Raises:
            ClientError
        """
        logger.info(self.__class__.__name__, "list", self.bucket, prefix)
        s3bucket = self._s3client.Bucket(self.bucket)
        summary = s3bucket.objects.filter(Prefix=('' if prefix is None else prefix))
        try:
            objects = list(summary)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return objects

    def upload_file(self, file_name, object_key=None):
        """Upload a file to an S3 bucket

        :param file_name: File to upload
        :param object_key: S3 object name. If not specified then file_name is used
        :return: True if file was uploaded, else False
        """
        logger.info(self.__class__.__name__, "upload_file", self.bucket, object_key)

        # If S3 object_key was not specified, use file_name
        if object_key is None:
            object_key = file_name

        try:
            self._s3client.upload_file(file_name, self.bucket, object_key)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            return True

    def check_and_create_prefix(self, prefix: str, create_prefix: bool = True):
        """ Check's if prefix exists and allows you to create it.

        :return: True | False
        """
        logger.info(self.__class__.__name__, f"Check if prefix {prefix} exists in bucket ", self.bucket)

        prefix = f"{prefix}/".replace("//", "/")  # ensures that we don't end up with //
        try:
            self._s3client.get_object(Bucket=self.bucket, Key=prefix)

        except ClientError as e:
            if e.response['ResponseMetadata']['HTTPStatusCode'] == const.STATUS_CODE_ERROR_S3_NOT_FOUND:
                logger.info(self.__class__.__name__, f"Prefix {prefix} does NOT exist in bucket ", self.bucket)
                if create_prefix:
                    logger.info(self.__class__.__name__, f"Creating prefix {prefix} in bucket ", self.bucket)
                    try:
                        self._s3client.put_object(Bucket=self.bucket, Key=prefix)
                    except Exception as e:
                        logger.error(self.__class__.__name__, f"Error creating prefix {prefix} in bucket ", self.bucket)
                        logger.error(self.__class__.__name__, e)
                        raise
                else:
                    return False
            else:
                logger.error(self.__class__.__name__, f"Unknown error while trying to access bucket in bucket ", self.bucket)
