"""AWS System Manager interface

Revisions:
    2020-09-19 San Brar    Initial version

"""

import boto3
from botocore.exceptions import ClientError
import utilities.constant as const
from utilities import utility
from utilities.logging.logger import logger


class SystemManagerClient:
    """
    Attributes:
        parameter (str): AWS System Manager parameter name
        parameter_value (str): AWS System Manager parameter value
    """

    def __init__(self):
        """Initialize class"""
        self._ssm_client = boto3.client('ssm', region_name=const.REGION_NAME)
        self.parameter = None
        self.parameter_value = None
        self.parameter_tags = None

    def get_parameter_value(self, parameter):
        """Get the value of the parameter
        Args:
            parameter (str): AWS System Manager parameter name
        Returns:
            str:
        Raises:
            ValueError, ClientError
        """
        self.parameter = parameter
        try:
            logger.info(self.__class__.__name__, "parameter", self.parameter)
            response = self._ssm_client.get_parameter(Name=self.parameter)
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
        else:
            self.parameter_value = response['Parameter']['Value']
            logger.info(self.__class__.__name__, "value", self.parameter_value)
            return self.parameter_value

    def put_parameter_value(self, parameter, value, description, parameter_type='String', overwrite=True, tags: list = None):
        """Put the value of the parameter
            See https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ssm.html#SSM.Client.put_parameter
            for details
        Args:
            parameter (str): AWS System Manager parameter name
            value (str): Parameter value
        Returns:
            str:
        Raises:
            ValueError, ClientError
            :param parameter_type:
            :param tags:
            :param overwrite:
            :param parameter:
            :param value:
            :param description:
        """
        self.parameter = parameter
        self.parameter_value = value
        try:
            logger.info(self.__class__.__name__, "parameter-put", self.parameter)

            tag_lst = tags + const.GLOBAL_TAGS if tags else const.GLOBAL_TAGS
            tag_lst = utility.create_key_value_list(in_dict_list=tag_lst)

            self._ssm_client.put_parameter(
                Name=self.parameter,
                Tags=tag_lst,
                Overwrite=overwrite,
                Description=description,
                Type=parameter_type
            )
        except ClientError as ex:
            logger.error(self.__class__.__name__, ex)
            raise
