"""AWS Lake Formation interface

Revisions:
    2020-12-09 Oliver Meyn    Initial version
"""

import boto3
from botocore.exceptions import ClientError
import utilities.constant as const
from utilities.logging.logger import logger


class LakeFormationClient():
    TABLE_READ_PERMISSIONS = ['SELECT', 'DESCRIBE']
    TABLE_ADMIN_PERMISSIONS = ['SELECT', 'DESCRIBE', 'INSERT', 'DELETE']
    DB_ADMIN_PERMISSIONS = ['CREATE_TABLE', 'DROP']
    S3_PERMISSIONS = ['DATA_LOCATION_ACCESS']

    def __init__(self):
        """Initialize class
        """
        self._lf_client = boto3.client('lakeformation', region_name=const.REGION_NAME)

    # S3 Locations
    def add_s3_location(self, full_path_arn):
        response = None
        try:
            response = self._lf_client.register_resource(
                ResourceArn=full_path_arn,
                UseServiceLinkedRole=True
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def add_s3_location_with_role(self, full_path_arn, custom_role_arn):
        response = None
        try:
            response = self._lf_client.register_resource(
                ResourceArn=full_path_arn,
                UseServiceLinkedRole=False,
                RoleArn=custom_role_arn
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def remove_s3_location(self, full_path_arn):
        response = None
        try:
            response = self._lf_client.deregister_resource(
                ResourceArn=full_path_arn
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def grant_s3_permissions(self, principal, full_path_arn):
        response = None
        try:
            response = self._lf_client.grant_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'DataLocation': {
                        'ResourceArn': full_path_arn
                    }
                },
                Permissions=self.S3_PERMISSIONS
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def revoke_s3_permissions(self, principal, full_path_arn):
        response = None
        try:
            response = self._lf_client.revoke_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'DataLocation': {
                        'ResourceArn': full_path_arn
                    }
                },
                Permissions=self.S3_PERMISSIONS
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    # Databases and Tables
    def list_database_permissions(self, principal, database):
        response = None
        try:
            response = self._lf_client.list_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Database': {
                        'Name': database
                    }
                },
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def list_all_tables_in_db_permissions(self, principal, database):
        response = None
        try:
            response = self._lf_client.list_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'TableWildcard': {}
                    }
                },
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def list_table_permissions(self, principal, database, table):
        response = None
        try:
            response = self._lf_client.list_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'Name': table
                    }
                },
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def grant_admin_on_database(self, principal, database):
        response = None
        try:
            response = self._lf_client.grant_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Database': {
                        'Name': database
                    }
                },
                Permissions=self.DB_ADMIN_PERMISSIONS
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def revoke_admin_on_database(self, principal, database):
        response = None
        try:
            response = self._lf_client.revoke_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Database': {
                        'Name': database
                    }
                },
                Permissions=self.DB_ADMIN_PERMISSIONS
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def __grant_on_table(self, principal, database, table, permissions):
        response = None
        try:
            response = self._lf_client.grant_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'Name': table
                    }
                },
                Permissions=permissions
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def __grant_on_all_tables(self, principal, database, permissions):
        response = None
        try:
            response = self._lf_client.grant_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'TableWildcard': {}
                    }
                },
                Permissions=permissions
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def __revoke_on_table(self, principal, database, table, permissions):
        response = None
        try:
            response = self._lf_client.revoke_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'Name': table
                    }
                },
                Permissions=permissions
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def __revoke_on_all_tables(self, principal, database, permissions):
        response = None
        try:
            response = self._lf_client.revoke_permissions(
                Principal={
                    'DataLakePrincipalIdentifier': principal
                },
                Resource={
                    'Table': {
                        'DatabaseName': database,
                        'TableWildcard': {}
                    }
                },
                Permissions=permissions
            )
        except ClientError as ex:
            logger.warn(self.__class__.__name__, ex)
        return response

    def grant_read_all_tables_in_db(self, principal, database):
        return self.__grant_on_all_tables(principal, database, self.TABLE_READ_PERMISSIONS)

    def revoke_read_all_tables_in_db(self, principal, database):
        return self.__revoke_on_all_tables(principal, database, self.TABLE_READ_PERMISSIONS)

    def grant_admin_all_tables_in_db(self, principal, database):
        return self.__grant_on_all_tables(principal, database, self.TABLE_ADMIN_PERMISSIONS)

    def revoke_admin_all_tables_in_db(self, principal, database):
        return self.__revoke_on_all_tables(principal, database, self.TABLE_ADMIN_PERMISSIONS)

    def grant_admin_single_table(self, principal, database, table):
        return self.__grant_on_table(principal, database, table, self.TABLE_ADMIN_PERMISSIONS)

    def revoke_admin_single_table(self, principal, database, table):
        return self.__revoke_on_table(principal, database, table, self.TABLE_ADMIN_PERMISSIONS)

    def grant_read_single_table(self, principal, database, table):
        return self.__grant_on_table(principal, database, table, self.TABLE_READ_PERMISSIONS)

    def revoke_read_single_table(self, principal, database, table):
        return self.__revoke_on_table(principal, database, table, self.TABLE_READ_PERMISSIONS)

