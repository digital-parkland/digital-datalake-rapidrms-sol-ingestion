""" Set of utilities

Revisions:
    2020-06-01 San Brar    Initial version
"""
##########################################################################
# Copyright 2020-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
# http://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the
# License for the specific language governing permissions and limitations under the License.
##########################################################################
import json
import logging
import subprocess, platform
import shutil
import boto3
import decimal
import os
from botocore.exceptions import ClientError
from urllib.parse import urlparse
from os import listdir
from os.path import isfile, join
from utilities import constant

py_logger = logging.getLogger(__name__)
py_logger.setLevel(os.getenv('LOGGING', 'DEBUG'))
REGION_NAME = os.getenv('AWS_REGION', 'us-west-2')


# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)


def get_app_version(filepath="_deploy.json"):
    # filepath = "_deploy.json"
    f = open(filepath, "r")
    file = json.load(f)
    return file['app_name']


def throw_if_null_object(obj, error_msg: str):
    # Immplement exception
    return None


def check_if_null_object(obj):
    if obj:
        if isinstance(obj, str):
            if obj.strip() != "":
                return False
            else:
                return True
        else:
            return False
    else:
        return True


def set_environmet_variable(env_var_name, env_var_value):
    """ Set OS Environment variable if it doesn't exists

    :param env_var:
    :return:
    """
    val = os.getenv(env_var_name)

    if val is None:
        os.environ[env_var_name] = str(env_var_value)


def make_archive(source, destination, archive_name):
    """
    Make zip of source folder to destination folder
    @param source:
    @param destination:
    @return:
    """
    shutil.make_archive(base_name=destination + "/" + archive_name, format='zip', root_dir=source)


def search_list_of_dict(dicts, key: str = '', value: str = ''):
    return next((item for item in dicts if item[key] == value), None)


def batch(iterable, n=1):
    """Divide list into chunks
    """
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]


def cloudwatch_log_msg_normalize(msg) -> str:
    """
    Converts multi line to single line
    @param msg:
    @return: str
    """

    return msg.replace("\n", "\r").replace("\r\r", "\r")


def read_json_file(file_path):
    with open(file_path) as f:
        data = json.load(f)
    return data


def dynamodb_table(table_name, region_name=REGION_NAME):
    """(str, str ) -> boto3.dynamodb.table    #**TypeContract**
        Return A resource representing an Amazon DynamoDB Table:   #**Description**
        dynamodb_table
    """
    dynamodb = boto3.resource('dynamodb', region_name=region_name)
    return dynamodb.Table(table_name)


def dynamodb_describe_table(table_name):
    try:
        response = dynamodb_client().describe_table(
            TableName=table_name
        )
        return response
    except ClientError as e:
        print(e)
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("======== Error - DynamoDB table not found ===========")
            return None
        else:
            raise e


def dynamodb_client(region_name=REGION_NAME):
    return boto3.client('dynamodb', region_name=region_name)


def glue_client(region_name=REGION_NAME):
    return boto3.client('glue', region_name=region_name)


def athena_client(region_name=REGION_NAME):
    return boto3.client('athena', region_name=region_name)


def stepfunction_client(region_name=REGION_NAME):
    return boto3.client('stepfunctions', region_name=region_name)


def s3_client(region_name=REGION_NAME):
    return boto3.client('s3', region_name=region_name)


class S3client(object):
    def __init__(self, url):
        self._parsed = urlparse(url, allow_fragments=False)
        self.S3client = boto3.resource('s3')

    @property
    def bucket(self):
        return self._parsed.netloc

    @property
    def key(self):
        if self._parsed.query:
            return self._parsed.path.lstrip('/') + '?' + self._parsed.query
        else:
            return self._parsed.path.lstrip('/')

    @property
    def url(self):
        return self._parsed.geturl()

    def getobject(self):
        print(self.bucket, self.key)
        s3object = self.S3client.Object(self.bucket, self.key)
        object_content = s3object.get()['Body'].read().decode('utf-8')
        return object_content


def get_glue_db(db_name):
    try:
        glueclient = glue_client()
        response = glueclient.get_database(Name=db_name)
        return response
    except ClientError as e:
        print(e)
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            print("======== Error - glue db not found ===========")
            return None


def get_glue_table(db_name, tbl_name):
    try:
        glueclient = glue_client()
        response = glueclient.get_table(DatabaseName=db_name, Name=tbl_name)
        return response
    except ClientError as e:
        print(e)
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            print("======== Error - glue db not found ===========")
            return None


def get_glue_crawler(crawler_name):
    try:
        glueclient = glue_client()
        response = glueclient.get_crawler(Name=crawler_name)
        return response
    except ClientError as e:
        print(e)
        if e.response['Error']['Code'] == 'EntityNotFoundException':
            print("======== Error - glue crawler not found ===========")
            return None


def create_glue_db(db_name, db_description):
    glueclient = glue_client()
    response = glueclient.create_database(
        DatabaseInput={
            'Name': db_name,
            'Description': db_description,
            # 'LocationUri': 'string',
            # 'Parameters': {
            #     'string': 'string'
            # },
            'CreateTableDefaultPermissions': [
                {
                    'Principal': {
                        'DataLakePrincipalIdentifier': 'IAM_ALLOWED_PRINCIPALS'
                    },
                    'Permissions': [
                        'ALL'
                    ]
                },
            ]
        }
    )


def secrets_client(region=REGION_NAME):
    return boto3.client('secretsmanager', region_name=region)


def get_secret(secret_name, log=py_logger, region_name=REGION_NAME):
    try:
        sm = secrets_client(region=region_name)
        log.info(f"==== Module: {__name__} - Get secret {secret_name} ====")
        secret = sm.get_secret_value(SecretId=secret_name)
        return secret
    except ClientError as e:
        log.error(f"==== Module: {__name__} - Get secret - client error ====")
        log.error(str(e))
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            return None
        else:
            log.error(f"====  Module: {__name__} - Error in getting secret {secret_name} ====")
            raise e


def create_secret(secret_name, secrets: dict, log=py_logger):
    try:
        log.info(f"==== Create secret {secret_name} ====")

        sm = secrets_client()
        response = sm.create_secret(
            Name=secret_name,
            SecretString=json.dumps(secrets))
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            return None
        else:
            log.error(f"==== Error in getting parameter {secret_name} ====")
            raise e


def get_aws_account_id(region_name=REGION_NAME):
    sts = boto3.client("sts", region_name=region_name, )
    # sts = boto3.client(
    #     "sts", region_name=region_name, aws_access_key_id=access_key, aws_secret_access_key=secret_key,
    # )  access_key=None, secret_key=None,

    user_arn = sts.get_caller_identity()["Arn"]
    return user_arn.split(":")[4]


def get_jdbc_source_boto3(connection_name):
    try:
        client_glue = glue_client()
        response = client_glue.get_connection(Name=connection_name)
    except Exception as e:
        print(e)
    else:
        if not response['Connection']:
            # raise Exception('Data source does not exist')
            return None
        else:
            return response['Connection']


def create_glue_jdbc_connection(connection_name, vpc_subnet_id, vpc_security_group_id,
                                update_existing_conn=True,
                                connectontype = 'JDBC',
                                connection_properties=None,
                                avalability_zone = constant.AVALABILITY_ZONE
                                ):
    """

    @param connection_name:
    @param vpc_subnet_id:
    @param vpc_security_group_id:
    @param update_existing_conn:
    @return:
    :param avalability_zone:
    :param connection_name:
    :param connectontype:
    :param connection_properties:
    """
    if connection_properties is None:
        connection_properties = {'JDBC_CONNECTION_URL': 'jdbc:sqlserver://ourhost:1433;databaseName=db1',
                                 'USERNAME': 'asdac2', 'PASSWORD': 'asdf23lkja0if'}
    gc = glue_client()

    glue_conn_source = get_jdbc_source_boto3(connection_name)

    if glue_conn_source and not update_existing_conn:
        # Delete existing connection
        response = gc.delete_connection(
            CatalogId=get_aws_account_id(),
            ConnectionName=connection_name
        )

    # if glue_conn_source and update_existing_conn:
    response = gc.create_connection(
        CatalogId=get_aws_account_id(),
        ConnectionInput={
            'Name': connection_name,
            # 'Description': 'Default glue connection to allow job to communicate to on prem db',
            'ConnectionType': connectontype,
            'ConnectionProperties': connection_properties,
            'PhysicalConnectionRequirements': {
                'SubnetId': vpc_subnet_id,
                'SecurityGroupIdList': [
                    vpc_security_group_id,
                ],
                'AvailabilityZone': avalability_zone
            }
        }
    )
    glue_conn_source = get_jdbc_source_boto3(connection_name)

    return glue_conn_source


def create_key_value_list(in_dict_list, remove_duplicate_keys=True, max_value_length=256):
    """Converts tags
        From: [{'tag1_key': 'tag1_value'}, ... ]
        To:   [{'key': 'tag1_key', 'value': 'tag1_value'}, ....]

    :param remove_duplicate_keys:
    :param max_value_length:
    :param in_dict_list:
    :return: List
    """
    # remove duplicates
    res_list = [i for n, i in enumerate(in_dict_list) if
                i not in in_dict_list[n + 1:]] if remove_duplicate_keys else in_dict_list

    keyvalue_list_out = []
    for tag in res_list:
        t = list(tag.items())
        value = t[0][1]

        if max_value_length:
            value = (value[:(max_value_length - 3)] + '..') if len(value) > max_value_length else value

        keyvalue_list_out.append({'Key': t[0][0], 'Value': value})

    return keyvalue_list_out


def create_kv_dict_list(in_dict_list):
    """Converts tags
        From: [{'key': 'tag1_key', 'value': 'tag1_value'}, ....]
        To:   [{'tag1_key': 'tag1_value'}, ... ]

    :param in_dict_list:
    :return: List
    """

    list_out = []
    for tag in in_dict_list:
        list_out.append({tag['Key']: tag['Value']})

    return list_out



def dms_client(region=REGION_NAME):
    return boto3.client('dms', region_name=region)


def get_parameter(parameter_name, log=py_logger):
    try:
        log.info(f"==== Get parameter {parameter_name} ====")
        ssm = ssm_client()
        parameter = ssm.get_parameter(Name=parameter_name, WithDecryption=True)
    except ClientError as e:
        if e.response['Error']['Code'] == 'ParameterNotFound':
            return None
        else:
            log.error(f"==== Error in getting parameter {parameter_name} ====")
            raise e


def create_parameter(name, description, value, param_type, log=py_logger):
    assert param_type in ['String', 'StringList', 'SecureString']
    try:
        log.info(f"==== Get parameter {name} ====")
        ssm = ssm_client()
        response = ssm.put_parameter(
            Name=name,
            Description=description,
            Value=value,
            Type=param_type
        )

    except ClientError as e:
        log.error(f"==== Error in getting parameter {name} ====")
        raise e


def ssm_client(region=REGION_NAME):
    return boto3.client('ssm', region_name=region)


def logs_client(region=REGION_NAME):
    return boto3.client('logs', region_name=region)


def create_log_group(log_gorup_name, retention_days=14, log=py_logger):
    cloudwatch_logs = logs_client()
    log.info(
        f"====== Module: {__name__} - crate log group {log_gorup_name} with retention {retention_days} days ======")
    try:
        response = cloudwatch_logs.describe_log_groups(
            logGroupNamePrefix=log_gorup_name,
            limit=2
        )

        if len(response['logGroups']) == 0:
            response = cloudwatch_logs.create_log_group(
                logGroupName=log_gorup_name
            )
            log.info(f"====== Module: {__name__} - log group creation response - {response} ======")

            response = cloudwatch_logs.put_retention_policy(
                logGroupName=log_gorup_name,
                retentionInDays=retention_days
            )

        print(response)
    except ClientError as e:
        log.error(f"==== Error in getting log group {log_gorup_name} ====")
        raise e


def get_objects_from_s3(bucket_name, prefix, log=py_logger):
    log.info(f"====== Module: {__name__} - get_objects objects in bucket {bucket_name} with prefix {prefix} ======")
    client = s3_client()
    s3_result = client.list_objects_v2(
        Bucket=bucket_name,
        Prefix=prefix
    )
    if 'Contents' not in s3_result:
        print(s3_result)
        log.info(f"====== Module: {__name__} - NO OBJECTS on prefix - {prefix} ======")
        return None

    object_list = s3_result['Contents']
    return object_list


def delete_objects_from_s3(bucket_name, prefix, log=py_logger):
    log.info(f"====== Module: {__name__} - Delete objects in bucket {bucket_name} with prefix {prefix} ======")
    client = s3_client()
    s3_result = client.list_objects_v2(
        Bucket=bucket_name,
        Prefix=prefix
    )
    if 'Contents' not in s3_result:
        print(s3_result)
        log.info(f"====== Module: {__name__} - NO OBJECTS DELETED on prefix - {prefix} ======")
        # print("********************* NO OBJECTS DELETED *********************")
        return

    object_list = s3_result['Contents']
    del_obj_list = []
    for o in object_list:
        log.info(f"====== Module: {__name__} - Delete object with key: {o['Key']} ======")
        del_obj_list.append({'Key': o['Key']})

    del_response = client.delete_objects(
        Bucket=bucket_name,
        Delete={'Objects': del_obj_list}
    )

    # print(s3_result)
    # print(del_response)

    while s3_result['IsTruncated']:
        continuation_key = s3_result['NextContinuationToken']
        s3_result = client.list_objects_v2(Bucket=bucket_name, Prefix=prefix, ContinuationToken=continuation_key)

        object_list = s3_result['Contents']
        del_obj_list = []
        for o in object_list:
            log.info(f"====== Module: {__name__} - Delete object with key: {o['Key']} ======")
            del_obj_list.append({'Key': o['Key']})

        del_response = client.delete_objects(
            Bucket=bucket_name,
            Delete={'Objects': del_obj_list}
        )

        # print(s3_result)
        print(del_response)

    pass


def os_get_only_files(my_path=None) -> list:
    """
    From current os path generate list of files. Will iterate over each folder.
        Excludes files starting with "." and "__pycache__"
        Exluces folders starting with "." and "_"
    """
    onlyfiles = []
    for f in listdir(my_path):
        ff = join(my_path, f) if my_path else f

        if isfile(ff):
            # if "__pycache__" not in ff and not ff.startswith("."):
            onlyfiles.append(ff.strip())
        # elif not ff.startswith(".") and not ff.startswith("_") and "__pycache__" not in ff:
        else:
            onlyfiles.extend(os_get_only_files(ff.strip()))

    return onlyfiles


# def check_and_create_s3_prefix(check_prefix: str, create_prefix: bool, s3_bucket: str, _s3_client=s3_client(),
#                                logger=py_logger):
#     """
#     This method is used to check the existence of version we want to deploy
#     :return: True | False
#     """
#     logger.info(f"====== Module: {__name__} - Check if prefix {check_prefix} exists in s3 bucket {s3_bucket} ======")
#
#     check_prefix = f"{check_prefix}/".replace("//", "/")  # ensures that we don't end up with //
#     try:
#         _s3_client.get_object(
#             Bucket=s3_bucket,
#             Key=check_prefix
#         )
#     except ClientError as e:
#         if e.response['ResponseMetadata']['HTTPStatusCode'] == 404:
#             logger.info(
#                 f"====== Module: {__name__} - Prefix {check_prefix} does NOT exists in s3 bucket {s3_bucket} ======")
#             if create_prefix:
#                 logger.info(f"====== Module: {__name__} - Creating prefix {check_prefix} in s3 bucket {s3_bucket} ======")
#                 try:
#                     _s3_client.put_object(Bucket=s3_bucket, Key=check_prefix)
#                 except Exception as e:
#                     logger.error(
#                         f"====== Module: {__name__} - Error while creating prefix {check_prefix} in s3 bucket {s3_bucket} ======")
#                     logger.error(f"====== Module: {__name__} - Error {e} ======")
#                     raise Exception(f"Unable to create the {check_prefix} in the bucket {s3_bucket}. Error: {str(e)}")
#             else:
#                 return False
#         else:
#             logger.error(f"====== Module: {__name__} - Error {e} ======")
#             raise Exception(f"Unknown error while trying to access bucket {s3_bucket}. Error: {str(e)}")
#
#
# def cf_client(region_name=REGION_NAME, pc_access_key=None, pc_secret_key=None):
#     client = boto3.client('cloudformation', region_name=region_name)
#
#     if pc_access_key:
#         client = boto3.client('cloudformation', region_name=region_name, aws_access_key_id=pc_access_key,
#                               aws_secret_access_key=pc_secret_key)
#
#     return client
#

def kms_client(region=REGION_NAME):
    return boto3.client('kms', region_name=region)


def get_cmk_arn(key_alias):
    client = kms_client()

    response = client.describe_key(
        KeyId=key_alias
    )

    return response.get('KeyMetadata').get('Arn')


def network_ping(hostname, log=py_logger):
    response = os.system("ping -c 1 " + hostname)
    log.info(
        f"====== Module: {__name__} - ping response {response} ======")
    # and then check the response...
    if response == 0:
        pingstatus = "Network Active"
    else:
        pingstatus = "Network Error"

    return pingstatus


def ping(server, count=1, wait_sec=1, log=py_logger):
    """
    :rtype: dict or None
    """

    log.info(f"====== Module: {__name__} - ping test on host: {server} ======")
    cmd = "ping -c {} -W {} {}".format(count, wait_sec, server).split(' ')
    try:
        output = subprocess.check_output(cmd).decode().strip()

        log.info(f"====== Module: {__name__} - ping test on host: {server} - result: {output} ======")

        lines = output.split("\n")
        total = lines[-2].split(',')[3].split()[1]
        loss = lines[-2].split(',')[2].split()[0]
        timing = lines[-1].split()[3].split('/')
        return {
            'type': 'rtt',
            'min': timing[0],
            'avg': timing[1],
            'max': timing[2],
            'mdev': timing[3],
            'total': total,
            'loss': loss,
        }
    except Exception as e:
        log.error(f"==== Error in trying to poing host {server}. Error:  {e} ====")
        return None


def host_port_connect(hostname, port, log=py_logger):
    import socket
    log.info(f"====== Module: {__name__} - socket test on host: {hostname} on port: {port} ======")
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        location = (hostname, port)
        a_socket.connect(location)

        # result_of_check = a_socket.connect_ex(location)
        # logger.info(f"====== Module: {__name__} - socket test results: {result_of_check} ======")
        # if result_of_check == 0:
        #     print("Port is open")
        # else:
        #     print("Port is not open")
    except Exception as e:
        log.error(f'==== Host and port test failed. Error: {e} ====')
        # raise NameError(f'-- host_port_connect -- {e}')
    finally:
        a_socket.close()
