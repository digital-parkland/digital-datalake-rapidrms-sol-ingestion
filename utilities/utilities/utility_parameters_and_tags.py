import json
from utilities.logging.logger import logger


def create_tag_file(tags: list):
    """
    Functions create tag file which is applied to all resources
    :return:
    """
    with open(f"stack_tags.json", 'w') as file_out:
        json.dump(tags, file_out)


def read_parameters(parameter_file):
    """Read parameter file

    :param parameter_file:
    :return:
    """
    logger.info("==== parse parameter from file ====")
    with open(parameter_file) as parameter_fileobj:
        parameter_data = json.load(parameter_fileobj)
    return parameter_data


def utf8len(s):
    return len(s.encode('utf-8'))


def create_cf_parameter_list(in_param_list, remove_duplicate_keys=True, max_bytes=4096):
    """Converts tags
        From: [{'param1_key': 'param1_value'}, ... ]
        To:   [{'ParameterKey': 'param1_key', 'ParameterValue': 'param1_value'}, ....]

    :param remove_duplicate_keys:
    :param max_bytes: Not used at this time
    :param in_param_list:
    :return: List
    """
    # remove duplicates
    res_list = [i for n, i in enumerate(in_param_list) if
                i not in in_param_list[n + 1:]] if remove_duplicate_keys else in_param_list

    out_param_list = []
    for param in res_list:
        t = list(param.items())
        value = t[0][1]
        key = t[0][0]

        if max_bytes and utf8len(value) > max_bytes:
            logger.error(__name__, f'CF Parameter value greater than {max_bytes}', )
            raise Exception(f'CF Parameter ({key}) value greater than {max_bytes}')

        out_param_list.append({'ParameterKey': key, 'ParameterValue': value})

    return out_param_list


def cf_parameter_list_to_list(in_param_list):
    """Converts tags
        From: [{'ParameterKey': 'param1_key', 'ParameterValue': 'param1_value'}, ....]
        To:   [{'param1_key': 'param1_value'}, ... ]

    :param in_param_list:
    :return: List
    """

    out_param_list = []
    for param in in_param_list:
        t = list(param.items())
        key = param.get('ParameterKey')
        value = param.get('ParameterValue')

        out_param_list.append({key: value})

    return out_param_list
