"""
Custom Glue logger
 - Logs custom data to S3
 - batches X number of logger entries
"""
from enum import Enum
import re
from pyspark.context import SparkContext
from utilities import utility_datetime
from pyspark.sql.types import ArrayType, StructField, StructType, StringType, IntegerType
import utilities.constant as const
import os
import utilities.utility_datetime as udt


class GlueLoggerLevel(Enum):
    INFO = 'info'
    ERROR = 'error'
    WARN = 'warn'
    DEBUG = 'debug'


# Create a schema for the dataframe
logger_schema = StructType([
    StructField('glue_job_id', StringType(), True),
    StructField('glue_run_id', StringType(), True),
    StructField('log_ts', StringType(), True),
    StructField('log_level', StringType(), True),
    StructField('log_msg', StringType(), True)
])


class GlueLogger:
    """Class for Glue logging utility"""
    def __init__(self, glue_job_name, glue_run_id, s3_bucket_name, s3_prefix,
                 sc: SparkContext, glue_db_name='datalake_ops', glue_job_id=None, logger=None):
        self._log = logger

        self.s3_bucket_name = s3_bucket_name
        self.s3_prefix = s3_prefix
        self.glue_job_name = glue_job_name
        self.glue_job_id = glue_job_id
        self.glue_run_id = glue_run_id
        self.sc: SparkContext = sc
        self.glue_db_name = glue_db_name
        self.log_list = []

        try:
            log_level_env = int(os.getenv(const.ETL_LOG_LEVEL_ENV))
        except (TypeError, ValueError):
            log_level_env = None

        self.log_level = max(
            const.ETL_LOG_LEVEL,
            const.ETL_LOG_LEVEL if log_level_env is None else log_level_env)

    def __print(self, s):
        """Print or logger using logger.warn"""
        msg = "{} {} {}".format(const.ETL_LOG_TAG, udt.now(), s)
        if self._log is None:
            print(msg)
        else:
            self._log.warn(msg)

    def debug(self, *argv):
        """Create debug logger"""
        l = list(argv)
        l.insert(0, '[DEBUG]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_DEBUG:
            self.__print(s)

        self.write_to_log(log_level=GlueLoggerLevel.DEBUG.value, log_msg=' '.join([str(i) for i in list(argv)]))

    def info(self, *argv):
        """Create info logger"""
        l = list(argv)
        l.insert(0, '[INFO]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_INFO:
            self.__print(s)

        self.write_to_log(log_level=GlueLoggerLevel.INFO.value, log_msg=' '.join([str(i) for i in list(argv)]))

    def warn(self, *argv):
        """Create warn logger"""
        l = list(argv)
        l.insert(0, '[WARN]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_WARN:
            self.__print(s)

        self.write_to_log(log_level=GlueLoggerLevel.WARN.value, log_msg=' '.join([str(i) for i in list(argv)]))

    def error(self, *argv):
        """Create error logger"""
        l = list(argv)
        l.insert(0, '[ERROR]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_ERROR:
            self.__print(s)

        self.write_to_log(log_level=GlueLoggerLevel.ERROR.value, log_msg=' '.join([str(i) for i in list(argv)]))

    def end(self):
        self.write_to_log(log_level=GlueLoggerLevel.INFO.value
                          , log_msg="=============== End of Logging ===============", write_to_file=True)

    def write_to_log(self, log_level, log_msg, write_to_file=False):
        """
        
        @param log_level: 
        @param log_msg: 
        @param write_to_file: 
        @return: 
        """

        glue_job_id = self.glue_job_id if self.glue_job_id else "1"

        # log_entry = (glue_job_id, self.glue_run_id, utility_datetime.now(), log_level,
        #              log_msg)  # log_msg.replace("\n", " ").replace("\r", " ")
        # self.log_list.append(log_entry)
        #
        # if len(self.log_list) >= 100 or write_to_file:
        #     from pyspark.sql import SQLContext
        #     sqlContext = SQLContext(self.sc)
        #
        #     job_name = re.sub(r'\W+', '',
        #                       self.glue_job_name.lower().replace(' ', '_').replace('-', '_').replace('__', '_'))
        #
        #     ###### Write to S3 ########
        #     # Convert list to RDD
        #     rdd = self.sc.parallelize(self.log_list)
        #     df = sqlContext.createDataFrame(rdd, logger_schema)
        #
        #     # print(str(self.log_list[0]))
        #     # df.printSchema()
        #     # df.show()
        #
        #     s3_path = "s3://" + f"{self.s3_bucket_name}/glue_job_logs/{job_name}/"
        #
        #     target_partition = "glue_job_id,glue_run_id"
        #
        #     # print(f"========== {s3_path} || {target_partition}  ==========")
        #
        #     df \
        #         .write \
        #         .mode('append') \
        #         .option("path", s3_path) \
        #         .saveAsTable(name=f"{self.glue_db_name}.{job_name}"
        #                      , format="parquet"
        #                      , mode='append'
        #                      , partitionBy=str(target_partition).split(",")
        #                      )
        #
        #     self.log_list = []
