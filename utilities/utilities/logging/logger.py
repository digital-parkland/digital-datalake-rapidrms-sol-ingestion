"""Custom ETL logging class

Revisions:
    2020-069-19 San    Initial version

"""
from botocore.exceptions import ClientError

import utilities.constant as const
import os
import utilities.utility_datetime as udt


class Logger:
    """Class for logging utility"""

    def __init__(self, logger=None):
        self._log = logger
        try:
            log_level_env = int(os.getenv(const.ETL_LOG_LEVEL_ENV))
        except (TypeError, ValueError):
            log_level_env = None
        self.log_level = max(
            const.ETL_LOG_LEVEL,
            const.ETL_LOG_LEVEL if log_level_env is None else log_level_env)

    def __print(self, s):
        """Print or logger using logger.warn"""
        msg = "{} {} {}".format(const.ETL_LOG_TAG, udt.now(), s)
        if self._log is None:
            print(msg)
        else:
            self._log.warn(msg)

    def debug(self, *argv):
        """Create debug logger"""
        l = list(argv)
        l.insert(0, '[DEBUG]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_DEBUG:
            self.__print(s)

    def info(self, *argv):
        """Create info logger"""
        l = list(argv)
        l.insert(0, '[INFO]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_INFO:
            self.__print(s)

    def warn(self, *argv):
        """Create warn logger"""
        l = list(argv)
        l.insert(0, '[WARN]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_WARN:
            self.__print(s)

    def error(self, *argv):
        """Create error logger"""
        l = list(argv)
        l.insert(0, '[ERROR]')
        s = ' '.join([str(i) for i in l])
        if self.log_level <= const.ETL_LOG_ERROR:
            self.__print(s)


def log_error(ex: Exception):
    """Log exception error message
    Args:
        ex (Exception):
    Returns:
        int: error code
        str: error message
    """
    if isinstance(ex, ClientError):
        error_code = const.STATUS_CODE_ERROR_BOTO_CLIENT
    elif isinstance(ex, ValueError):
        error_code = const.STATUS_CODE_ERROR_VALUE
    elif isinstance(ex, KeyError):
        error_code = const.STATUS_CODE_ERROR_KEY
    else:
        error_code = const.STATUS_CODE_ERROR_OTHERS
    message = "E{}: {}".format(error_code, str(ex))
    logger.error(message)
    return error_code, message


logger = Logger()
