from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobrunstatus import JobRunStatus
from utilities.utility import glue_client
from utilities import constant
from botocore.exceptions import ClientError

def run_glue_job(gclient = None, region=constant.REGION_NAME, env=constant.ENVIRONMENT,
                 etl_job: EtlJob = None, job_id=None, glue_job_name=None, last_run_status=None,
                 raise_error_on_max_concurrent=True):

    try:
        if gclient is None:
            gclient = glue_client()

        if job_id is None:
            job_id = etl_job.job_id
            last_run_status = etl_job.last_run_status
            glue_job_name=etl_job.function_name

        # Check if job is currently running
        if last_run_status == JobRunStatus.RUNNING.value:
            print(
                f"====== Job ({job_id}) currently in {last_run_status} state. Job will not be executed ======")
        else:
            job_parameters = {
                "--region": region,
                "--environment": env,
                "--etl_job_id": job_id,
                '--enable-continuous-cloudwatch-log': 'true',
                '--enable-continuous-log-filter': 'true',
                '--enable-glue-datacatalog': 'true',
                '--enable-metrics': 'true',
                '--enable-s3-parquet-optimized-committer': 'true'
            }

            response = gclient.start_job_run(
                JobName=glue_job_name,
                Arguments=job_parameters,
                Timeout=constant.DEFAULT_GLUE_JOB_TIMEOUT
            )
            print(f"ran etl job - job id: {job_id} - {response}")
    except ClientError as ex:
        if raise_error_on_max_concurrent == False and ex.response['Error']['Code'] == 'ConcurrentRunsExceededException':
            print(ex)
            return "ConcurrentRunsExceededException"
            pass
        else:
            raise ex
