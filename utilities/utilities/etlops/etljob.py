"""ETL Operations - etl job

Revisions:
    2020-09-19 San Brar    Initial version

"""
##########################################################################
# Copyright 2020-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
# http://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the
# License for the specific language governing permissions and limitations under the License.
##########################################################################
"""Job object

Object used to store metadata for job
    Job Types [Package, Glue, Lambda, Redshift, Batch, EMR]
"""
import json
from copy import copy
from datetime import datetime
from botocore.exceptions import ClientError
from croniter import croniter
from utilities import constant as const
from utilities import utility_datetime
from utilities.etlops.etljobrunstatus import JobRunStatus
from utilities.etlops.etljobtype import JobType
from utilities.utility import dynamodb_table, DecimalEncoder, cloudwatch_log_msg_normalize
from utilities.logging.logger import Logger


class EtlJob:

    def __init__(self, job_id=None, log=Logger(), json_dic=None):
        self.log = log

        self.job_id = job_id
        self.name = None
        self.description = None
        self.job_type: JobType = JobType.GLUE
        self.jira_num = None
        self.status = "active"
        self.cron = None  # https://en.wikipedia.org/wiki/Cron space del
        self.function_name = ""
        self.predecessor = None  # Do not start until parent job(s) have been completed
        self.successor = None  # Trigger successor jobs after completion of current job
        self.successor_can_start = 0
        self.package_id = None  # If defined this job is child of the package job
        self.last_update_ts = utility_datetime.now()
        self.last_run_start_ts = utility_datetime.now()
        self.last_run_end_ts = ""
        self.last_run_status = JobRunStatus.NEW.value
        self.last_run_id = ""
        self.error_cnt = "0"
        self.job_args: dict = {}  # Job arguments - will depend on the job what arguments are needed.
        self.job_args_before_run: dict = {}  # Copy of Job arguments - made using execute start of job process.

        # Update values from dic
        if json_dic:
            self.from_json(json_dic=json_dic)
        elif job_id:
            self.get_item_by_key()

        self.log.info(cloudwatch_log_msg_normalize(
            f"====== Module: {__name__} -  ETL Job Object initialized {self.to_json()} ======"))

    @property
    def job_type(self):
        return self._job_type

    @job_type.setter
    def job_type(self, value):
        if not isinstance(value, JobType):
            raise TypeError('job_type must be an JobType')
        self._job_type = value

    def add_item(self):
        try:
            self.last_update_ts = utility_datetime.now()

            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOB_TBL())
            val = str(self.to_json())

            self.log.info(f"====== Module: {__name__} -  DynamoDb put with job iD: {self.job_id}")
            val_dic = json.loads(val)
            response = ddb.put_item(Item=val_dic)
            self.log.info(f"====== Module: {__name__} -  {response}")

        except ClientError as e:
            self.log.error(f"====== Module: {__name__} -  {e}")
            raise Exception(e.response['Error']['Message'])

    def get_item_by_key(self, job_id=None):
        """
        Get job information from dynamodb table using the job_id.
        :param job_id: Optional, if entered overwrites object's job_id
        :return: None
        """
        if job_id:
            self.job_id = job_id

        try:
            response = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOB_TBL()).get_item(
                Key={
                    'job_id': self.job_id
                }
            )
        except ClientError as e:
            self.log.error(f"====== Module: {__name__} -  {e.response['Error']['Message']}")
            raise Exception(f"self.job_id : Key not found in DynamoDB job table ({const.ETL_OPS_DYNAMODB_ETLJOB_TBL()}). Error: {str(e)}")
        else:
            item = response.get('Item')
            if item:
                # self.log.info(cloudwatch_log_msg_normalize(
                #     f"====== Module: {__name__} - etl_job: {self.job_id} value from db: {str(json.dumps(item, indent=4, cls=DecimalEncoder))} ======"))
                self.from_json(json_dic=item)
                return True
            else:
                self.log.error(
                    f"====== Module: {__name__} - etl job_id {self.job_id} not found in in the DynamoDB table 'etl_job'. Get Item response: {json.dumps(item, indent=4, cls=DecimalEncoder)} ======")
                # raise Exception(self.job_id + " Key (job_id) not found in DynamoDB table 'etl_job'.")
                return False

    def get_previous_scheduled_run_ts(self, base_datetime: datetime = utility_datetime.now_ts()):
        """
        Based on cron expresion determine previous scheduled run.
        @param base_datetime: datetime next scheduled compared to a reference datetime.
        @return: str 
        """
        cron_iter = croniter(self.cron, base_datetime)
        previous_datetime = cron_iter.get_prev(datetime)
        return utility_datetime.to_string(previous_datetime)

    def get_next_scheduled_run_ts(self, base_datetime: datetime = utility_datetime.now_ts()):
        """
        Based on cron expresion determine next scheduled run.        
        @param base_datetime: datetime next scheduled compared to a reference datetime.
        @return: str 
        """
        cron_iter = croniter(self.cron, base_datetime)
        next_datetime = cron_iter.get_next(datetime)
        return utility_datetime.to_string(next_datetime)

    def add_job_to_queue(self):
        """
        Execute steps taken during a job run
        @return:
        """
        self.last_run_start_ts = ""
        self.last_run_end_ts = ""
        self.last_run_id = ""
        self.last_run_status = JobRunStatus.PENDING.value
        self.add_item()

        self.log.info(
            f"====== Module: {__name__} - etl_job: {self.job_id} - Job add to queue ======")

    def execute_job_start_process(self, last_jobrun_id):
        """
        Execute steps taken during a job run
        @return:
        """
        self.last_run_start_ts = utility_datetime.now()  # Update the etl run ts - regardless of success - this fields records that job ran at this time.kld
        self.last_run_end_ts = ""
        self.last_run_id = copy(last_jobrun_id)
        self.last_run_status = JobRunStatus.RUNNING.value
        self.job_args_before_run = copy(self.job_args)
        self.add_item()

        self.log.info(
            f"====== Module: {__name__} - etl_job: {self.job_id} - Job Started ======")

    def execute_job_end_process(self, status):
        """
        Run job start steps
        @return:
        """
        if status == JobRunStatus.SUCCESS.value:
            self.error_cnt_clear()
        elif status == JobRunStatus.ERROR.value:
            self.error_cnt_inc()

        self.last_run_end_ts = utility_datetime.now()
        self.last_run_status = status
        self.add_item()

        self.log.info(
            f"====== Module: {__name__} - etl_job: {self.job_id} - Job Ended ======")

    def error_cnt_inc(self, inc=1):
        """ Increment error count

        :param inc: Default to 1
        :return:
        """
        self.error_cnt = str(int(self.error_cnt) + inc)

    def error_cnt_clear(self):
        self.error_cnt = "0"

    def to_json(self) -> str:
        """
        Returns current object in json formatted string
        :return:
        """
        filtered = {k: v for k, v in self.__dict__.items() if
                    (v is not None and k != "_job_type" and k != "logger" and k != "log")}
        j = json.dumps(filtered, indent=4, cls=DecimalEncoder)
        json_obj = j[:len(j) - 1] + \
                   ', "job_type": "' + str(self.job_type.value) + '"' + \
                   '}'
        return json_obj

    def from_json(self, json_str: str = None, json_dic=None):

        if json_str:
            json_dic = json.loads(json_str)

        allowed_keys = {'job_id', 'name', 'description', 'cron', 'predecessor', 'successor', 'status',
                        'successor_can_start', 'package_id', 'last_update_ts', 'job_args', 'job_args_before_run',
                        'last_run_start_ts', 'last_run_end_ts', 'last_run_status', 'last_run_id', 'function_name',
                        'jira_num', 'error_cnt'}
        self.__dict__.update((k, v) for k, v in json_dic.items() if k in allowed_keys)

        job_type = json_dic.get('job_type')
        self.job_type = JobType(job_type) if job_type else None
