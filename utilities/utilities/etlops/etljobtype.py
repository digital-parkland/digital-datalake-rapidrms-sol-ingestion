"""ETL Operations

Revisions:
    2020-09-19 San Brar    Initial version

"""
from enum import Enum


class JobType(Enum):
    WORKGROUP = 'workgroup'  # Job contains set child jobs that can be run in parallel
    GLUE = 'glue'  # glue jobs
    LAMBDA = 'lambda'  # aws lambda jobs
    DMS = 'dms'  # aws lambda jobs
    STEPFUNCTION = 'stepfunction'  # aws step function
