from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
import sys
import os
from utilities import utility_datetime
from utilities.clients.sns import SnsClient
from utilities.clients.ssm import SystemManagerClient
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobrunstatus import JobRunStatus
from utilities.logging.gluelogger import GlueLogger


class GlueSparkJobUtil:

    def __init__(self):
        print(f"====== Job Start at  {utility_datetime.now()}  ====== ")
        self.spark_context = SparkContext()
        self.glue_context = GlueContext(self.spark_context)
        self.spark_session = self.glue_context.spark_session.builder.master('local[*]').getOrCreate()
        self.glue_job = Job(self.glue_context)  # for Job Bookmarks

        # Get job arguments
        self.args = getResolvedOptions(sys.argv,
                                       ['JOB_NAME',
                                        'etl_job_id',
                                        'environment',
                                        'region'])

        print(f"====== Args - {self.args}  ======")

        self.glue_job.init(self.args['JOB_NAME'], self.args)  # Initialise the glue job
        self.glue_job_run_id = self.args['JOB_RUN_ID']  # Job id is passed in as command line argument

        self.environment = self.args['environment']
        self.region_name = self.args['region']
        etl_job_id = self.args['etl_job_id']

        # assert (etl_job_id is not None, "Job arguments must include etl_job_id.")
        # assert (self.environment is not None, "Job arguments must include environment.")
        # assert (self.region_name is not None, "Job arguments must include region.")

        # ########### Set global region and environment variables
        os.environ["environment"] = self.environment
        r_name = os.getenv('AWS_REGION', None)
        if r_name is None:
            os.environ['AWS_REGION'] = self.region_name

        # ########### set clients
        self.ssm_client = SystemManagerClient()
        self.sns_client = SnsClient(
            sns_arn=self.ssm_client.get_parameter_value(f'/{self.environment}/{self.region_name}/datalake/sns/operations/arn'))

        self.logger = GlueLogger(
            glue_job_name=self.args['JOB_NAME'],
            glue_run_id=self.glue_job_run_id,
            glue_job_id=etl_job_id,
            sc=self.spark_context,
            s3_bucket_name=self.ssm_client.get_parameter_value(f'/{self.environment}/{self.region_name}/datalake/s3/log/name'),
            s3_prefix="datalake_ops"
        )

        self.logger.info(f"Job Start at  {utility_datetime.now()}  ====== ")
        self.logger.info(f"Args - {self.args}  ======")
        self.logger.info(f"job name - {self.args['JOB_NAME']}  ======")

        ########################################################################################
        # Get etl job info

        self.logger.info(f"====== Job ID - {etl_job_id} get job information from the etl_job table======")

        self.etl_job = EtlJob(job_id=etl_job_id, log=self.logger)

        # If last run still running notify and exit
        if self.etl_job.last_run_status == JobRunStatus.RUNNING.value:
            self.logger.error(
                f"====== Job ({self.etl_job.job_id}) currently in running state. Job will not be executed ======")
            self.sns_client.publish(
                msg=f"Glue job error. Job ID: {etl_job_id} and job run id: {self.glue_job_run_id} . \n Instance of the job is already running. Running job's run id: {self.etl_job.last_run_id}")
            self.logger.error(f"====== Done - Error ======")
            self.logger.end()
            raise Exception(
                f"====== Job ({self.etl_job.job_id}) in running state since {self.etl_job.last_run_start_ts}. Job will not be executed ======")

        # assert (self.etl_job.function_name is not None, "Not valid ETL Job table.")
        # assert (self.etl_job.function_name != "", "Not valid ETL Job table.")

        # Execute job start process
        self.etl_job.execute_job_start_process(last_jobrun_id=self.glue_job_run_id)

    def exit_success(self):
        self.glue_job.commit()
        self.etl_job.execute_job_end_process(JobRunStatus.SUCCESS.value)
        self.logger.info(f"====== Done - Success ======")
        self.logger.end()

    def exit_error(self, ex: Exception):
        self.logger.error(f"====== Module: {__name__} -  {ex}")
        self.etl_job.execute_job_end_process(JobRunStatus.ERROR.value)

        # Send notification
        self.sns_client.publish(
            msg=f"Glue job error. Job ID: {self.etl_job.job_id} and glue job run id: {self.glue_job_run_id}. ||| error: {ex}")

        self.logger.error(f"====== Done - Error ======")
        self.logger.end()
