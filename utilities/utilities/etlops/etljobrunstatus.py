"""ETL Operations

Revisions:
    2020-09-19 San Brar    Initial version

"""
from enum import Enum


class JobRunStatus(Enum):
    NEW = 'new'  # Job run object just created - pending initialization
    PRE_CONFIG = 'pre-config'  # Job in pre config
    PENDING = 'pending'  # Job is pending processing
    RUNNING = 'running'  # Job is currently running
    ABORTED = 'aborted'  # JOB is aborted
    SUCCESS = 'success'  # job completed with success
    ERROR = 'error' # job encountered an error while running
