import os
from utilities import utility_datetime
from utilities.clients.sns import SnsClient
from utilities.clients.ssm import SystemManagerClient
from utilities.etlops.etljob import EtlJob
from utilities.etlops.etljobrunstatus import JobRunStatus
from utilities.logging.logger import Logger
from utilities import constant as const


class LambdaJobUtil():

    def __init__(self, context):
        const.ETL_LOG_TAG = f'=============== {context.aws_request_id}'
        self.logger = Logger()

        self.logger.info(f"Job Start at  {utility_datetime.now()}")
        self.lambda_job_run_id = context.aws_request_id

        self.environment = os.getenv('environment')
        self.region_name = os.getenv('AWS_REGION')  # https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-runtime
        etl_job_id = os.getenv('etl_job_id')

        # ########### set clients
        self.ssm_client = SystemManagerClient()
        self.sns_client = SnsClient(
            sns_arn=self.ssm_client.get_parameter_value(f'/{self.environment}/{self.region_name}/datalake/sns/operations/arn'))

        self.logger.info(f"Job Start at  {utility_datetime.now()}")

        ########################################################################################
        # Get etl job info

        self.logger.info(f"====== Job ID - {etl_job_id} get job information from the etl_job table======")
        self.etl_job = EtlJob(job_id=etl_job_id, log=self.logger)

        # # Execute job start process ### DO NOT auto run this for Lambda
        # self.etl_job.execute_job_start_process(last_jobrun_id=self.lambda_job_run_id)

    def exit_success(self):
        self.etl_job.execute_job_end_process(JobRunStatus.SUCCESS.value)
        self.logger.info(f"====== Done - Success ======")

    def exit_error(self, ex: Exception):
        self.logger.error(f"====== Module: {__name__} -  {ex}")
        self.etl_job.execute_job_end_process(JobRunStatus.ERROR.value)

        # Send notification
        self.sns_client.publish(
            msg=f"Glue job error. Job ID: {self.etl_job.job_id} and lambda job run id (aws_request_id): {self.lambda_job_run_id}. ||| error: {ex}")

        self.logger.error(f"====== Done - Error ======")

    def check_if_running(self, raise_error=True, reset_status_after_x_mins=None):
        # If last run still running notify and exit
        if self.etl_job.last_run_status == JobRunStatus.RUNNING.value:
            self.logger.error(
                f"====== Job ({self.etl_job.job_id}) currently in running state. ======")
            self.sns_client.publish(
                msg=f"Glue job error. Job ID: {self.etl_job.job_id} and lambda job run id (aws_request_id): {self.lambda_job_run_id} . \n Instance of the job is already running. Running job's run id: {self.etl_job.last_run_id}")
            self.logger.error(f"====== Done - Error ======")
            if raise_error:
                raise Exception(
                    f"====== Job ({self.etl_job.job_id}) in running state since {self.etl_job.last_run_start_ts}. Job will not be executed ======")



