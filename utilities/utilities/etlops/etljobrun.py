"""ETL Operations - etl job ru

Revisions:
    2020-09-19 San Brar    Initial version

"""
##########################################################################
# Copyright 2020-2020 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
# http://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied. See the
# License for the specific language governing permissions and limitations under the License.
##########################################################################
"""ETL Job

Object used to store metadata for the job.


How will the queue attribute will be used?
 queue = "0" -> will be last run of the job
 queue = "1" -> will be current run of the job


 What should job run track?
  - snapshot of the job config -- contains a copy of the entire etl job
  - number of records (inserted, updated, deleted)
  - lineage
        - source tables to target tables
        - Mappings if avaliable

"""
import logging
import os
import json
from copy import copy

from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr
from utilities.utility import dynamodb_table, DecimalEncoder
from utilities import utility_datetime
from utilities.etlops.etljobrunstatus import JobRunStatus
from utilities.etlops.etljob import EtlJob
from utilities import constant as const 

QUEUEING_INDEX_NAME = os.getenv('etl_job_run_queueing_index_name', 'queued-last_upd_ts-index')

py_logger = logging.getLogger(__name__)
py_logger.setLevel(os.getenv('logging', 'DEBUG'))


class EtlJobRun:

    def __init__(self, job: EtlJob, log=py_logger,
                 jobrun_id=None, last_upd_ts=utility_datetime.now(), status: JobRunStatus = JobRunStatus.NEW,
                 next_run_ts=None,
                 creation_ts=utility_datetime.now(), details=None,
                 queued=None, version="1", pre_check="0", post_check="0", dlq=None,
                 json_dic=None
                 ):
        self.log = log
        self.job: EtlJob = copy(
            job)  # ######## Make a point in time copy of the etl_job object not just reference. This will allow us to get a snapshot of the etl_job object.
        self.jobrun_id = jobrun_id if jobrun_id else job.job_id + "|" + utility_datetime.now()
        self.last_upd_ts = last_upd_ts
        self.status: JobRunStatus = status
        self.next_run_ts = None  # next_run_ts if next_run_ts else job.get_next_scheduled_run_ts()
        self.creation_ts = creation_ts
        self.details = details
        self.queued = queued
        self.version = version  # Not used at this time but could be used if multiple consumers/processors
        self.pre_check = pre_check
        self.post_check = post_check
        self.dlq = dlq
        self.run_start_ts = utility_datetime.now()
        self.run_end_ts = None
        self.metrics = {"inserted": "0", "updated": "0", "deleted": "0", "run_seconds": "0"}
        self.qa_metrics = {"source_count": "0", "target_count": "0"}
        self.lineage = {"source": "", "target": ""}

        if json_dic:
            self.from_json(json_dic=json_dic)

        self.log.info(f"====== Module: {__name__} -  ETL JobRun Object initialized {self.to_json()} ======")

    def add_item(self):
        self.log.info(f"====== Module: {__name__} -  ETL Job Run Object add item - {self.to_json()} ======")
        try:
            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL)
            self.last_upd_ts = utility_datetime.now()
            val = self.to_json()

            print(f'Table {const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL}, attempting to add item: {val}')
            print(f'Job instance key is {self.jobrun_id}')

            val_dic = json.loads(val)
            response = ddb.put_item(Item=val_dic)
        except ClientError as e:
            raise Exception(e.response['Error']['Message'])

    def increment_metrics(self, inserted=0, updated=0, deleted=0):
        """
        Increment metric
        @param inserted:
        @param updated:
        @param deleted:
        @return:
        """

        ins = int(self.metrics.get('inserted')) + inserted
        upd = int(self.metrics.get('updated')) + updated
        dele = int(self.metrics.get('deleted')) + deleted

        self.last_upd_ts = utility_datetime.now()
        self.metrics['inserted'] = str(ins)
        self.metrics['updated'] = str(upd)
        self.metrics['deleted'] = str(dele)
        self.log.info(f"====== Module: {__name__} -  increment_metrics - {self.to_json()} ======")
        self.add_item()

    def peek(self):
        """
        Retrieve from the queue and start processing the job.
        Change status to "Processing"
        :return: None
        """

    def remove(self, status: JobRunStatus):
        """
        Remove the object from queue (queued = 0) and update status
        :param status:     str
        :return:
        """
        self.queued = None
        self.status = status
        self.add_item()

    def update_status(self, status: JobRunStatus):
        """
        Update job run's status

        :param status:  str
        :return:
        """
        self.status = status
        self.last_upd_ts = utility_datetime.now()
        self.add_item()

    def enqueue(self, pre_check_flag):
        """
        Put the item in queue and update the pre_check_flag
        :param pre_check_flg:
        :return:
        """

    def register_job(self):
        try:

            self.last_upd_ts = utility_datetime.now()
            self.creation_ts = utility_datetime.now()
            self.status = JobRunStatus.RUNNING
            self.add_item()

        except ClientError as e:
            raise Exception("Register to PipelineLogging Failed in register_job")
        else:
            return self.jobrun_id

    def is_in_run_queue(self) -> bool:
        """
        Check if job is in the job run queue using jobrun_id
            If in queue then update self value by the val retrieved from db
        :return:
        """
        item_in_queue = self.get_job_in_queue(job_id=self.job.job_id, queue="1")

        # Update self from from db info
        if item_in_queue:
            self.from_json(json_dic=item_in_queue)
            return True
        return False

    def get_item_by_key(self, job_run_id=None):
        """
        Get job information from dynamodb table using the jobrun_id.
        :param job_run_id: Optional, if entered overwrites object's jobrun_id
        :return: None
        """
        if job_run_id:
            self.jobrun_id = job_run_id

        try:
            response = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL).get_item(
                Key={
                    'jobrun_id': self.jobrun_id
                }
            )
        except ClientError as e:
            self.log.info(f"====== Module: {__name__} -  {e.response['Error']['Message']}")
            raise Exception(self.jobrun_id + " Key not found in DynamoDB job table " + str(e))
        else:
            item = response.get('Item')
            if item:
                self.log.info(
                    f"====== Module: {__name__} - etl_job_run : {self.jobrun_id} value from db: {json.dumps(item, indent=4, cls=DecimalEncoder)} ======")
                self.from_json(json_dic=item)
                return True
            else:
                self.log.error(
                    f"====== Module: {__name__} - etl jobrun_id {self.jobrun_id} not found in in the DynamoDB table 'etl_jobrun'. Get Item response: {json.dumps(item, indent=4, cls=DecimalEncoder)} ======")
                raise Exception(self.jobrun_id + " Key (jobrun_id) not found in DynamoDB table 'etl_job_run'.")

    @staticmethod
    def get_job_in_queue(job_id: str, queue: str = "1") -> dict:
        """
        Check if job with given id is etl job run queue
        :param job_id:
        :param queue: 1 = current queue and 0 = last run queue
        :return:
        """
        try:
            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL)  # self.etl_ops.jobrun_tbl)
            response = ddb.query(
                IndexName=QUEUEING_INDEX_NAME,
                ScanIndexForward=False,
                KeyConditionExpression=Key('queued').eq(queue),
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Unable to get item by job id.")
        else:
            # print(str(response))
            items = response.get('Items', None)

            if items:
                # search_list_of_dict the list of items for the job
                for i in items:
                    if i['job']['jobrun_id'] == job_id:
                        print('Job found in queue')
                        return i

            # job not found in queue
            print(f'Job {job_id} not found in queue.')
            return None

    def check_predecessor(self):
        """
        Check if predecessors are in queue or last run was success
            If not then put predecessors in queue - if allowed by predecessor

            How to view a valid previous instance of the job run?
            Last run of the job must have completed between the next scheduled instances and previous
            scheduled instance of the job.
                - Only check the stats of this instance of the job run.

        What should be done if failure?

        :return:
        """
        # TODO: To be implemented -- no business case of checking predecessors at this time.

        # Get list of predecessors
        for p in self.job.predecessor.split(','):
            # Validate p
            p_job_status = "Pending"
            p_job = EtlJob()
            p_job.get_item_by_key(job_id=p.trim())

            # Check if predecessors are in queue
            p_in_run_queue = self.get_job_in_queue(job_id=p_job.job_id, queue="1")

            if p_in_run_queue:
                p_job_status = "Scheduled"
            else:
                # Get last run status
                p_in_last_run_queue = self.get_job_in_queue(job_id=p_job.job_id, queue="0")

                if p_in_last_run_queue:

                    # Check if last run of the job within bounds of the
                    p_job_run = EtlJobRun(json_dic=p_in_last_run_queue, job=None)

                    p_previous_scheduled_run_ts = p_job.get_previous_scheduled_run_ts()
                    p_next_scheduled_run_ts = p_job.get_next_scheduled_run_ts()
                    p_last_upd_ts = p_job_run.last_upd_ts

                    p_is_last_run_n_minus_1_run = utility_datetime.between(p_previous_scheduled_run_ts,
                                                                           p_next_scheduled_run_ts, p_last_upd_ts)

                    # Check if last run is

                    # Check status
                    if not p_is_last_run_n_minus_1_run:
                        p_job_status = "Not Run"
                    elif p_job_run.status == JobRunStatus.SUCCESS:
                        p_job_status = "Success"
                    else:
                        p_job_status = "Error"
                else:
                    p_job_status = "Not Run"

                print(p_job_status)

        return True

    def update_job_success(self, jobtype):
        """
        Update job status to sucess
        @param jobtype:
        @return:
        """
        try:
            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL)  # self.etl_ops.jobrun_tbl)
            response = ddb.update_item(
                Key={
                    'JobKey': self.jobrun_id,
                    'Name': self.job.job_id
                },
                UpdateExpression="set EndTime=:t, #sts=:s, lastUpdTime=:t, #jt=:jtype",
                ExpressionAttributeValues={
                    ':t': utility_datetime.now(),
                    ':s': "Success",
                    ':jtype': jobtype
                },
                ExpressionAttributeNames={
                    "#sts": "Status",
                    "#jt": "JobType"},
                ReturnValues="UPDATED_NEW"
            )
            print(response, jobtype)
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Update to PipelineLogging with Status Failed in update_job_success")
        else:
            return response

    def update_job_failed(self, errormsg, jobtype):
        """
        Update job status to failure
        @param errormsg:
        @param jobtype:
        @return:
        """
        try:
            print('***********update job failed**********')
            print(self.jobrun_id, self.job.job_id, errormsg)
            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL)  # self.etl_ops.jobrun_tbl)
            response = ddb.update_item(
                Key={
                    'JobKey': self.jobrun_id,
                    'Name': self.job.job_id
                },
                UpdateExpression="set EndTime=:t, #sts=:s, lastUpdTime=:t, #err=:e, #jt=:jtype",
                ExpressionAttributeValues={
                    ':t': utility_datetime.now(),
                    ':s': "Failed",
                    ':e': str(errormsg),
                    ':jtype': jobtype
                },
                ExpressionAttributeNames={
                    "#sts": "Status",
                    "#err": "Error",
                    "#jt": "JobType"},
                ReturnValues="UPDATED_NEW"
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Update to PipelineLogging with Status Failed in update_job_failed")
        else:
            return response

    def get_last_run(self):
        """
        Get last run details of the job
        @return:
        """
        
        try:
            ddb = dynamodb_table(const.ETL_OPS_DYNAMODB_ETLJOBRUN_TBL)  # self.etl_ops.jobrun_tbl)
            response = ddb.query(
                IndexName='PipelinelastUpdTime-index',
                ScanIndexForward=False,
                KeyConditionExpression=Key('Name').eq(self.job.job_id),
                FilterExpression=Attr("Status").eq('Success'),
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
            raise Exception("Unable to get last run date in PipelineLogging")
        else:
            if response.get('Items', None):
                return response['Items'][0]['last_upd_ts']
            else:
                return None

    def execute_job_start_process(self):
        """
        Run job start steps
        @return:
        """
        self.last_upd_ts = utility_datetime.now()
        self.run_start_ts = utility_datetime.now()
        self.run_end_ts = ""
        self.status = JobRunStatus.RUNNING
        self.add_item()

        self.log.info(
            f"====== Module: {__name__} - etl_job: {self.jobrun_id} - Job Run start process ======")
        self.log.info(
            f"====== Module: {__name__} - etl_job - Job start process - {str(self.to_json())} ======")

    def execute_job_end_process(self, status: JobRunStatus, msg=None):
        """
        Run job start steps
        @return:
        """
        self.last_upd_ts = utility_datetime.now()
        self.run_end_ts = utility_datetime.now()
        self.status = status
        if msg:
            self.details = msg

        # Update job end timestamp
        self.metrics['run_seconds'] = str(utility_datetime.seconds_between_ts(self.run_start_ts, self.run_end_ts))
        self.add_item()

        self.log.info(
            f"====== Module: {__name__} - etl_job: {self.jobrun_id} - Job Run end process ======")
        self.log.info(
            f"====== Module: {__name__} - etl_job - Job end process - {str(self.to_json())} ======")

    def to_json(self) -> str:
        """
        Returns current object in json formatted string
        :return:
        """
        filtered = {k: v for k, v in self.__dict__.items()
                    if (v is not None and k != "job" and k != "logger" and k != "status")}

        j = json.dumps(filtered, indent=4, cls=DecimalEncoder)

        json_obj = j[:len(j) - 1] + ', "status": "' + str(self.status.value) + '"' + ', "job": ' + self.job.to_json() + '}'
        return json_obj

    def from_json(self, json_str: str = None, json_dic=None):

        if json_str:
            json_dic = json.loads(json_str)

        allowed_keys = {'jobrun_id', 'last_upd_ts', 'next_run_ts', 'creation_ts', 'details', 'queued',
                        'version', 'pre_check', 'post_check', 'dlq', 'metrics', 'qa_metrics', 'lineage', 'run_start_ts',
                        'run_end_ts'}
        self.__dict__.update((k, v) for k, v in json_dic.items() if k in allowed_keys)

        status = json_dic.get('status')
        self.status = JobRunStatus(status) if status else None

        job = json_dic.get('job')
        self.job = EtlJob(json_dic=job) if job else None
