"""Global constants and configuration

Revisions:
    2020-09-19 San    Initial version

"""
import os

# Environment
ENVIRONMENT = os.getenv('environment', os.getenv('ENVIRONMENT'))  # Should error if not set.
REGION_NAME = os.getenv('AWS_REGION', 'us-west-2')
AVALABILITY_ZONE = os.getenv('AVALABILITY_ZONE', 'us-west-2a')
AWS_S3_URL = 'https://s3.amazonaws.com'
S3_PREFIX = 'pki'
ROLE_PREFIX = 'pki'
APPROVED_ENVIRONMENTS = ['dev', 'test', 'prod', 'preprod']
JDE_SERVER_TIMEZONE_OFFSET = -7 # OFFSET is from the UTC -- currently JDE server is located in mountian time.
# Tags
GLOBAL_TAGS = [{'BusinessUnit': '12078'},  # Default business unit
               {'BillingAccount': 'Digital'},  # Default billing account
               {'AppRole': 'data_process'},  # The functional role of the associated AWS resource.
               {'Customer': 'data_analytics'},  # Team/Customer associated to the given AWS resource.
               {'Owner': 'digitalteam@parkland.ca'},
               # The email address of owners who are associated with the given AWS resource, for sending outage notifications.
               {'DataSteward': 'digitalteam@parkland.ca'},
               # The email address of data stewart who are associated with the given AWS resource, for sending outage, data quality and access notifications.
               {'Confidentiality': 'confidential'},
               # Type of data to be stored, processed or transmitted associated with the AWS resource.
               {'GitRepo': 'digital-infrastructure'},
               # Each solution must include repository name
               {'GitBranch': 'master'},
               # Each solution must include branch name
               {'GitHash': 'ABCDEFGHI..........'},
               # Each solution must include commit hash
               ]
TAGS_APP_ROLE_ALLOWED_VALUES = ['data_storage', 'monitoring', 'logging', 'data_process', 'data_ingestion', 'network',
                                'identity']
TAGS_CONFIDENTIALITY_ALLOWED_VALUES = ['System', 'Public', 'Internal', 'Confidential', 'Protected']

# Logging level and setting
# export ETL_LOG_LEVEL=4 to mute
ETL_LOG_LEVEL_ENV = 'ETL_LOG_LEVEL'
ETL_LOG_LEVEL = 1
ETL_LOG_DEBUG = 0
ETL_LOG_INFO = 1
ETL_LOG_WARN = 2
ETL_LOG_ERROR = 3
ETL_LOG_TAG = '==============='

# Return status code
STATUS_CODE_OK = 200
STATUS_CODE_ERROR_BOTO_CLIENT = 460
STATUS_CODE_ERROR_DATABASE = 490
STATUS_CODE_ERROR_JOB_STARTED = 491
STATUS_CODE_ERROR_KEY = 497
STATUS_CODE_ERROR_VALUE = 498
STATUS_CODE_ERROR_OTHERS = 499
STATUS_CODE_ERROR_S3_NOT_FOUND = 404

# CloudFormation
KMS_TEMPLATE_STACK_NAME = "digital-datalake-kms"
S3_TEMPLATE_STACK_NAME = "digital-datalake-s3"
TEMPLATES_CONFIG_FILE = './config/deploy_config.json'


def GLOBAL_CF_PARAMETERS():
    return [
        {'pEnvironment': f'{ENVIRONMENT}'},  # Default ENVIRONMENT parameter
    ]


CF_NO_UPDATE_ERROR = "No updates are to be performed."
CF_WAITER_DELAY = 30  # IN SECONDS
CF_WAITER_MAX_ATTEMPTS = 120

CF_UPDATE_STACK_SUCCESS_LIST = ['UPDATE_COMPLETE']
CF_UPDATE_STACK_FAILURE_LIST = ['UPDATE_ROLLBACK_FAILED', 'UPDATE_ROLLBACK_COMPLETE']
CF_UPDATE_STACK_WATIER_NAME = 'stack_update_complete'

CF_CREATE_STACK_SUCCESS_LIST = ['CREATE_COMPLETE']
CF_CREATE_STACK_FAILURE_LIST = ['ROLLBACK_FAILED', 'ROLLBACK_COMPLETE']
CF_CREATE_STACK_WAITER_NAME = 'stack_create_complete'

CF_DELETE_STACK_WAITER_NAME = 'stack_delete_complete'
CF_DELETE_STACK_SUCCESS_LIST = ['DELETE_COMPLETE']
CF_DELETE_STACK_FAILURE_LIST = ['DELETE_FAILED']
CF_DELETE_STACK_IF_IN_STATUS = ['CREATE_FAILED', 'ROLLBACK_FAILED', 'DELETE_FAILED', 'ROLLBACK_COMPLETE']

CF_STACK_EXISTS_LIST = ['CREATE_COMPLETE', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_FAILED',
                        'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'UPDATE_ROLLBACK_COMPLETE']
CF_STACK_NOT_EXISTS_LIST = ['DELETE_COMPLETE']

CF_STACK_IN_MID_PROCESSING_LIST = ['ROLLBACK_IN_PROGRESS', 'DELETE_IN_PROGRESS', 'CREATE_IN_PROGRESS',
                                   'UPDATE_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
                                   'UPDATE_ROLLBACK_IN_PROGRESS', 'REVIEW_IN_PROGRESS']


# Artifact Deployment
def DEFAULT_ARTIFACT_S3_BUCKET(): return f"{S3_PREFIX}-datalake-artifacts-{ENVIRONMENT}"


def DEFAULT_ARTIFACT_PREFIX(): return f"artifacts_{ENVIRONMENT}/ca/parkland"


ARTIFACT_VERSION_CONFIG_FILE_NAME = "_version.json"  # Contains the current version of the application >>> {"version": "0.0.1"}
ARTIFACT_DEPLOY_CONFIG_FILE_NAME = "_deploy.json"  # Version to deploy and application name >> {"deployment_version": "Latest", "app_name": "ingest-user-data"}
ARTIFACT_BUILD_IGNORE_LIST = ['.*', '__pycache__', 'venv']
UTILITIES_APP_NAME = 'digital-infrastructure'  # Project/app name of where utilities project lives. --


# Default ETL config
def ETL_OPS_DYNAMODB_ETLJOB_TBL(): return f"etl_job_{os.getenv('environment', os.getenv('ENVIRONMENT'))}"


def ETL_OPS_DYNAMODB_ETLJOB_INDEX1(): return f'job_type-status-index'


def ETL_OPS_DYNAMODB_ETLJOBRUN_TBL(): return f"etl_job_run_{os.getenv('environment', os.getenv('ENVIRONMENT'))}"


DEFAULT_SPARK_PARTITIONS = 20
DEFAULT_PARQUET_COMPRESSION = 'snappy'
DEFAULT_START_TIME = '1970-01-01T00:00:00.000000'
DEFAULT_DATE_FORMAT = "yyyy-MM-dd"
DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
DEFAULT_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSSSS"

DEFAULT_ETL_JOB_TABLE = 'etl_job'
DEFAULT_ETL_JOB_RUN_TABLE = 'etl_job_run'

# Data Catalogue
MAX_GLUE_PARTITION_DELETE = 25
MAX_GLUE_PARTITION_CREATE = 100

DEFAULT_GLUE_JOB_TIMEOUT = 2280  # IN SECONDS
