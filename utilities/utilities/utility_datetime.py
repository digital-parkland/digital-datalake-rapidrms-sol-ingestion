""" Set of datetime utilities

Revisions:
    2020-06-01 San Brar    Initial version
"""
# https://howchoo.com/g/ywi5m2vkodk/working-with-datetime-objects-and-timezones-in-python
# Tenants
# 1) always use "aware" datetime objects. - Timezone aware
# 2) always work in UTC and do timezone conversion as a last step

import datetime
import logging
import os
from math import ceil
from datetime import timedelta

py_logger = logging.getLogger(__name__)
py_logger.setLevel(os.getenv('logging', 'DEBUG'))


def now() -> str:
    return datetime.datetime.utcnow().isoformat()


def now_ts() -> datetime:
    return datetime.datetime.utcnow()


def format_datetime(dt: datetime) -> str:
    return dt.isoformat()


def to_string(dt, sep="T") -> str:
    if isinstance(dt, datetime.datetime):
        return dt.isoformat(sep=sep)
    elif isinstance(dt, datetime.date):
        return dt.isoformat()
    else:
        str(dt)


def to_datetime(ts: str) -> datetime:
    if len(ts) == 10:
        ts_format = "%Y-%m-%d"
    elif "." in ts and len(ts) > 19:
        ts_format = "%Y-%m-%dT%H:%M:%S.%f" if "T" in ts else "%Y-%m-%d %H:%M:%S.%f"
    else:
        ts_format = "%Y-%m-%dT%H:%M:%S" if "T" in ts else "%Y-%m-%d %H:%M:%S"

    return datetime.datetime.strptime(ts, ts_format)


def seconds_between_ts(start_ts, end_ts):
    sts = start_ts if isinstance(start_ts, datetime.datetime) else to_datetime(start_ts)
    ets = end_ts if isinstance(end_ts, datetime.datetime) else to_datetime(end_ts)

    return (ets - sts).total_seconds()


def to_jde_julian_date(dt):

    if not isinstance(dt, datetime.datetime):
        dt = to_datetime(dt)

    cy = dt.year - 1900
    day_of_year = dt.timetuple().tm_yday
    return f'{cy:03}{day_of_year:03}'


def to_date_from_jde_julian_date(jde_dt):
    jdeDT = f'000{jde_dt}'[-6:]
    dt_str = f"{int(jdeDT[0:3]) + 1900}-01-01T00:00:00.000"

    dt = to_datetime(dt_str)

    days = int(jdeDT[-3:])

    return dt + timedelta(days=days - 1)


def between(start_ts, end_ts, compare_ts) -> bool:
    """
    Is compare_ts between the start and end ts
    :param start_ts:
    :param end_ts:
    :param compare_ts:
    :return: bool
    """

    sts = to_datetime(start_ts) if type(start_ts) == 'str' else start_ts
    ets = to_datetime(end_ts) if type(end_ts) == 'str' else end_ts
    cts = to_datetime(compare_ts) if type(compare_ts) == 'str' else compare_ts

    if sts <= cts <= ets:
        return True
    else:
        return False


def batch_datetime_v0(lower_bound_date: datetime, upper_bound_date: datetime, batch_size_in_days=10):
    delta_days = (upper_bound_date - lower_bound_date).days
    print(delta_days)

    num_of_batches = ceil(delta_days / batch_size_in_days) + 1
    print(num_of_batches)

    for i in range(num_of_batches):

        b = lower_bound_date + datetime.timedelta(days=(batch_size_in_days * i))

        print((upper_bound_date - b).days)

        if b > upper_bound_date or (upper_bound_date - b).days <= 0:
            b = upper_bound_date
            print(f"i = {i} --- b = {str(b)}")
            return
        print(f"i = {i} --- b = {str(b)}")
        # print(b)


def batch_datetime(start_datetime: datetime, end_datetime: datetime, batch_size_in_days=10, log=py_logger):
    delta_days = (end_datetime - start_datetime).days
    # print(delta_days)

    log.info(f"====== partitioned_extract - Create batches ======")
    if delta_days <= batch_size_in_days:
        yield (start_datetime, end_datetime)
    else:
        for i in range(0, delta_days, batch_size_in_days):
            b = start_datetime + datetime.timedelta(days=(i))
            c = start_datetime + datetime.timedelta(days=(i + batch_size_in_days))

            # Safety check for c
            if c > end_datetime or (end_datetime - c).days <= 0:
                c = end_datetime

            # print(f"i = {i} --- b = {str(b)} : c = {c}")
            yield (b, c)
